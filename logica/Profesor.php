<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ProfesorDAO.php";

class Profesor{
    private $idProfesor;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $estado;
    private $conexion;
    private $profesorDAO;
    
    
    public function getIdProfesor()
    {
        return $this->idProfesor;
    }
    
    public function getNombre()
    {
        return $this->nombre;
    }
    
    public function getApellido()
    {
        return $this->apellido;
    }
    
    public function getCorreo()
    {
        return $this->correo;
    }
    
    public function getClave()
    {
        return $this->clave;
    }
    
    
    public function getEstado()
    {
        return $this->estado;
    }
    
    public function Profesor($idProfesor= "", $nombre = "", $apellido = "", $correo = "", $clave = "",$estado= "" ){
        $this -> idProfesor = $idProfesor;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> profesorDAO = new  ProfesorDAO($this -> idProfesor, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this -> estado);
    }
    
    
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> profesorDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> estado = $resultado[3];
        
    }
    public function consultar1(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> profesorDAO -> consultar1());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idProfesor = $resultado[0];
        $this -> nombre = $resultado[1];
        $this -> apellido = $resultado[2];
        $this -> correo = $resultado[3];
        $this -> estado = $resultado[4];
        
    }
    
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> profesorDAO-> consultarTodos());
        $profesores = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Profesor($resultado[0], $resultado[1], $resultado[2], $resultado[3], "", $resultado[4]);
            array_push($profesores, $p);
        }
        $this -> conexion -> cerrar();
        return $profesores;
    }
    
    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> profesorDAO -> existeCorreo());
        $this -> conexion -> cerrar();
        return $this -> conexion -> numFilas();
    }
    
    
    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> profesorDAO -> registrar());
        $this -> conexion -> cerrar();
    }
    
    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> profesorDAO -> autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idProfesor = $resultado[0];
            $this -> estado = $resultado[1];
            return true;
        }else {
            return false;
        }
    }
    
    //     public function actualizarDatos(){
    //         $this -> conexion -> abrir();
    //         $this -> conexion -> ejecutar($this -> administradorDAO ->actualizarDatos());
    //         $this -> conexion -> cerrar();
    //     }
    
    
    //     public function actualizarClave(){
    //         $this -> conexion -> abrir();
    //         $this -> conexion -> ejecutar($this -> administradorDAO ->actualizarClave());
    //         $this -> conexion -> cerrar();
    //     }
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> profesorDAO -> consultarPaginacion($cantidad, $pagina));
        $proyectos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            //$idProfesor= "", $nombre = "", $apellido = "", $correo = "", $clave = "",$estado= ""
            $p = new Profesor($resultado[0], $resultado[1], $resultado[2], $resultado[3],"",$resultado[4]);
            array_push($proyectos, $p);
        }
        $this -> conexion -> cerrar();
        return $proyectos;
    }
    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> profesorDAO -> consultarFiltro($filtro));
        $proyectos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            //$idProfesor= "", $nombre = "", $apellido = "", $correo = "", $clave = "",$estado= ""
            $p = new Profesor($resultado[0], $resultado[1], $resultado[2], $resultado[3],"",$resultado[4]);
            array_push($proyectos, $p);
        }
        $this -> conexion -> cerrar();
        return $proyectos;
    }
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> profesorDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    public function cambiarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> profesorDAO -> cambiarEstado());
        $this -> conexion -> cerrar();
    }
    
    
    
    
}

?>
