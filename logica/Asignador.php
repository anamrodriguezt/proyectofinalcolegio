<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/AsignadorDAO.php";
class Asignador{
    private $idAsignador;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $estado;
    private $conexion;
    private $administradorDAO;

    public function getIdAsignador(){
        return $this -> idAsignador;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getApellido(){
        return $this -> apellido;
    }

    public function getCorreo(){
        return $this -> correo;
    }

    public function getClave(){
        return $this -> clave;
    }

    public function getEstado(){
        return $this -> estado;
    }
    
    public function Asignador($idAsignador= "", $nombre = "", $apellido = "", $correo = "", $clave = "",$estado = ""){
        $this -> idAsignador = $idAsignador;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> estado= $estado;
        $this -> conexion = new Conexion();
        $this -> asignadorDAO = new AsignadorDAO($this -> idAsignador, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this -> estado);
    }

    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> asignadorDAO -> autenticar());
        $this -> conexion -> cerrar();       
        if ($this -> conexion -> numFilas() == 1){            
            $resultado = $this -> conexion -> extraer();
            $this -> idAsignador = $resultado[0];   
            $this -> estado = $resultado[1];
            return true;        
        }else {
            return false;
        }
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> asignadorDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> estado= $resultado[3];
    }
    public function consultar1(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> asignadorDAO -> consultar1());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idAsignador = $resultado[0];
        $this -> nombre = $resultado[1];
        $this -> apellido = $resultado[2];
        $this -> correo = $resultado[3];
        $this -> estado = $resultado[4];
        
    }
    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> asignadorDAO -> existeCorreo());
        $this -> conexion -> cerrar();
        return $this -> conexion -> numFilas();
    }
    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> asignadorDAO -> registrar());
        $this -> conexion -> cerrar();
    }
    

//     public function actualizarDatos(){
//         $this -> conexion -> abrir();
//         $this -> conexion -> ejecutar($this -> administradorDAO ->actualizarDatos());
//         $this -> conexion -> cerrar();
//     }
    
    
//     public function actualizarClave(){
//         $this -> conexion -> abrir();
//         $this -> conexion -> ejecutar($this -> administradorDAO ->actualizarClave());
//         $this -> conexion -> cerrar();
//     }
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> asignadorDAO -> consultarPaginacion($cantidad, $pagina));
        $asignadores = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            //$idAsignador= "", $nombre = "", $apellido = "", $correo = "", $clave = "",$estado = ""
            $p = new Asignador($resultado[0], $resultado[1], $resultado[2], $resultado[3],"",$resultado[4]);
            array_push($asignadores, $p);
        }
        $this -> conexion -> cerrar();
        return $asignadores;
    }
    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> asignadorDAO -> consultarFiltro($filtro));
        $asignadores = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            //$idProfesor= "", $nombre = "", $apellido = "", $correo = "", $clave = "",$estado= ""
            $p = new Asignador($resultado[0], $resultado[1], $resultado[2], $resultado[3],"",$resultado[4]);
            array_push($asignadores, $p);
        }
        $this -> conexion -> cerrar();
        return $asignadores;
    }
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> asignadorDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    public function cambiarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> asignadorDAO -> cambiarEstado());
        $this -> conexion -> cerrar();
    }
    
}

?>