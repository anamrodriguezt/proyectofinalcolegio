<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ComentarioDAO.php";
class Comentario{
    private $idComentario;
    private $texto;
    private $proyecto;
    private $fecha;
    private $conexion;
    private $ComentarioDAO;

    

    /**
     * @return string
     */
    public function getIdComentario()
    {
        return $this->idComentario;
    }
    public function getFecha()
    {
        return $this->fecha;
    }
    /**
     * @return mixed
     */
    public function getTexto()
    {
        return $this->texto;
    }

    public function getProyecto()
    {
        return $this->proyecto;
    }

    public function Comentario($idComentario= "", $texto= "", $proyecto= "",$fecha= ""){
        $this -> idComentario= $idComentario;
        $this -> texto = $texto;
        $this -> proyecto = $proyecto;
        $this -> fecha = $fecha;
        
        $this -> conexion = new Conexion();
        $this -> comentarioDAO = new ComentarioDAO($this -> idComentario, $this -> texto, $this -> proyecto,$this -> fecha);
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> comentarioDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> texto = $resultado[0];
        $this -> fecha = $resultado[1];
        }

        public function insertar(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> comentarioDAO-> insertar());
            $this -> conexion -> cerrar();
        }
        
        public function consultarTodos(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> comentarioDAO-> consultarTodos());
            $estudiantes = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
                $e = new Comentario("",$resultado[0], "", $resultado[1]);
                array_push($estudiantes, $e);
            }
            $this -> conexion -> cerrar();
            return $estudiantes;
        }
    
}

?>