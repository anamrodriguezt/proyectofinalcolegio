<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/LogTutorDAO.php";
class LogTutor{
    private $id;
    private $accion;
    private $datos;
    private $fecha;
    private $hora;
    private $tutor;
    private $conexion;
    private $logTutorDAO;

    public function getId()
    {
        return $this->id;
    }
    public function getAccion()
    {
        return $this->accion;
    }

    public function getDatos()
    {
        return $this->datos;
    }
 
    public function getFecha()
    {
        return $this->fecha;
    }

    public function getHora()
    {
        return $this->hora;
    }
    
    public function getTutor()
    {
        return $this->tutor;
    }
    
    public function LogTutor($id = "", $accion= "", $datos = "", $fecha= "", $hora= "", $tutor= "" ){
        $this -> id = $id;
        $this -> accion = $accion;
        $this -> datos = $datos;
        $this -> fecha = $fecha;
        $this -> hora = $hora;
        $this -> tutor= $tutor;
        $this -> conexion = new Conexion();
        $this -> logTutorDAO= new LogTutorDAO($this -> id, $this -> accion, $this -> datos, $this -> fecha, $this -> hora, $this -> tutor);
    }

     public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logTutorDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> accion = $resultado[0];
        $this -> datos = $resultado[1];
        $this -> fecha = $resultado[2];
        $this -> hora = $resultado[3];
        $this -> tutor = $resultado[4];
        
     }
    
    
    
}

?>