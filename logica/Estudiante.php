<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/EstudianteDAO.php";
class Estudiante{
    private $idEstudiante;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $codigo;
    private $estado;
    private $conexion;
    private $estudianteDAO;
    
    
   public function getIdEstudiante()
    {
        return $this->idEstudiante;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getApellido()
    {
        return $this->apellido;
    }

    public function getCorreo()
    {
        return $this->correo;
    }

    public function getClave()
    {
        return $this->clave;
    }

    public function getCodigo()
    {
        return $this->codigo;
    }

    public function getEstado()
    {
        return $this->estado;
    }


    public function Estudiante($idEstudiante= "", $nombre = "", $apellido = "", $correo = "", $clave = "",$codigo = "",$estado= "" ){
        $this -> idEstudiante = $idEstudiante;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> codigo = $codigo;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> estudianteDAO = new  EstudianteDAO($this -> idEstudiante, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this -> codigo,$this -> estado);
    }
    
    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteDAO -> autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idEstudiante = $resultado[0];
            $this -> estado = $resultado[1];
            return true;
        }else {
            return false;
        }
    }
    
    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteDAO-> existeCorreo());
        $this -> conexion -> cerrar();
        return $this -> conexion -> numFilas();
    }
    public function consultar1(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteDAO -> consultar1());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idEstudiante = $resultado[0];
        $this -> nombre = $resultado[1];
        $this -> apellido = $resultado[2];
        $this -> correo = $resultado[3];
        $this -> codigo = $resultado[4];
        $this -> estado = $resultado[5];
        
    }
    
    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteDAO-> registrar());
        $this -> conexion -> cerrar();
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        if($resultado!=""){
            $this -> idEstudiante= $resultado[0];
            $this -> nombre = $resultado[1];
            $this -> apellido = $resultado[2];
            $this -> correo = $resultado[3];
            $this -> codigo = $resultado[4];
            $this -> estado = $resultado[5];
            return 1;
        }else{
            return 0;
        }
       
    }
    
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteDAO-> consultarTodos());
        $estudiantes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $e = new Estudiante($resultado[0], $resultado[1], $resultado[2], $resultado[3], "", $resultado[4],$resultado[5]);
            array_push($estudiantes, $e);
        }
        $this -> conexion -> cerrar();
        return $estudiantes;
    }
    
    
    public function consultarEstudiante(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteDAO-> consultarEstudiante());
        $estudiantes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $e = new Estudiante($resultado[0], $resultado[1], $resultado[2], $resultado[3], "", $resultado[4],$resultado[5]);
            array_push($estudiantes, $e);
        }
        $this -> conexion -> cerrar();
        return $estudiantes;
    }
    
//     public function actualizarDatos(){
//         $this -> conexion -> abrir();
//         $this -> conexion -> ejecutar($this -> administradorDAO ->actualizarDatos());
//         $this -> conexion -> cerrar();
//     }
    
    
//     public function actualizarClave(){
//         $this -> conexion -> abrir();
//         $this -> conexion -> ejecutar($this -> administradorDAO ->actualizarClave());
//         $this -> conexion -> cerrar();
//     }
    public function cambiarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteDAO -> cambiarEstado());
        $this -> conexion -> cerrar();
    }
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteDAO -> consultarPaginacion($cantidad, $pagina));
        $estudiantes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            //$idEstudiante= "", $nombre = "", $apellido = "", $correo = "", $clave = "",$codigo = "",$estado= ""
            $e = new Estudiante($resultado[0], $resultado[1], $resultado[2], $resultado[3],"",$resultado[4],$resultado[5]);
            array_push($estudiantes, $e);
        }
        $this -> conexion -> cerrar();
        return $estudiantes;
    }
    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteDAO -> consultarFiltro($filtro));
        $proyectos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            //$idEstudiante= "", $nombre = "", $apellido = "", $correo = "", $clave = "",$codigo = "",$estado= ""
            //Id,nombre, apellido,correo,codigo,estado
            $e = new Estudiante($resultado[0], $resultado[1], $resultado[2], $resultado[3],"",$resultado[4],$resultado[5]);
            array_push($proyectos, $e);
        }
        $this -> conexion -> cerrar();
        return $proyectos;
    }
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    
    
}

?>