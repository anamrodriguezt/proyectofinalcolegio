<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ProyectoDAO.php";
class Proyecto{
    private $idProyecto;
    private $titulo;
    private $descripcion;
    private $pdf;
    private $estudiante;
    private $estudiante2;
    private $tutor;
    private $juez;
    private $estado;
    private $conexion;
    private $proyectoDAO;

    
    
    /**
     * @return mixed
     */
    public function getIdProyecto()
    {
        return $this->idProyecto;
    }

    /**
     * @return mixed
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @return mixed
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * @return mixed
     */
    public function getTutor()
    {
        return $this->tutor;
    }

    /**
     * @return mixed
     */
    public function getJurado()
    {
        return $this->juez;
    }

    /**
     * @return mixed
     */
    public function getEstudiante()
    {
        return $this->estudiante;
    }

    public function getEstado()
    {
        return $this->estado;
    }
    
    public function getEstudiante2()
    {
        return $this->estudiante2;
    }
    
    
    
    public function Proyecto($idProyecto= "", $titulo= "", $descripcion= "", $pdf= "",$estudiante= "",$estudiante2= "", $tutor= "",$juez= "",$estado= ""){
        $this -> idProyecto = $idProyecto;
        $this -> titulo = $titulo;
        $this -> descripcion = $descripcion;
        $this -> pdf = $pdf;
        $this -> estudiante = $estudiante;
        $this -> estudiante2 = $estudiante2;
        $this -> tutor = $tutor;
        $this -> juez = $juez;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> proyectoDAO = new ProyectoDAO($this -> idProyecto, $this -> titulo, $this -> descripcion, $this -> pdf,$this -> estudiante,$this -> estudiante2, $this -> tutor,$this -> juez,$this -> estado);
    }
    function consultar2(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO -> consultar2());
        $resultado = $this -> conexion -> extraer();
        $this -> idProyecto = $resultado[0];
        $this -> titulo = $resultado[1];
        $this -> descripcion = $resultado[2];
        $this -> pdf = $resultado[3];
        $this -> estudiante = $resultado[4];
        $this -> estudiante2 = $resultado[5];
        $this -> tutor = $resultado[6];
        $this -> juez= $resultado[7];
        $this -> estado= $resultado[8];
        $this -> conexion -> cerrar();
        return $resultado[0];
    }
    function consultar3(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO -> consultar2());
        $resultado = $this -> conexion -> extraer();
        if($resultado!=""){
            $this -> idProyecto = $resultado[0];
            $this -> titulo = $resultado[1];
            $this -> descripcion = $resultado[2];
            $this -> pdf = $resultado[3];
            $this -> estudiante = $resultado[4];
            $this -> estudiante2 = $resultado[5];
            $this -> tutor = $resultado[6];
            $this -> juez= $resultado[7];
            $this -> estado= $resultado[8];
            $this -> conexion -> cerrar();
        return 1;
        }else{
            return 0;
        }
    }
    function consultar4($estudiante){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO -> consultar4($estudiante));
        $resultado = $this -> conexion -> extraer();
        if($resultado!=""){
            $this -> idProyecto = $resultado[0];
            $this -> titulo = $resultado[1];
            $this -> descripcion = $resultado[2];
            $this -> pdf = $resultado[3];
            $this -> estudiante = $resultado[4];
            $this -> estudiante2 = $resultado[5];
            $this -> tutor = $resultado[6];
            $this -> juez= $resultado[7];
            $this -> estado= $resultado[8];
            $this -> conexion -> cerrar();
            return 1;
        }else{
            return 0;
        }
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> idProyecto = $resultado[0];
        $this -> titulo = $resultado[1];
        $this -> descripcion = $resultado[2];
        $this -> pdf = $resultado[3];
        $this -> estudiante = $resultado[4];
        $this -> estudiante2 = $resultado[5];
        $this -> tutor = $resultado[6];
        $this -> juez= $resultado[7];
        $this -> estado= $resultado[8];
        $this -> conexion -> cerrar();
    }
    
    public function consultarPJ(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO-> consultarPJ());
        $estudiantes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $e = new Proyecto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4],$resultado[5],$resultado[6],"",$resultado[7]);
            array_push($estudiantes, $e);
        }
        $this -> conexion -> cerrar();
        return $estudiantes;
    }
    public function consultarPT(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO-> consultarPT());
        $estudiantes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $e = new Proyecto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4],$resultado[5],"",$resultado[6],$resultado[7]);
            array_push($estudiantes, $e);
        }
        $this -> conexion -> cerrar();
        return $estudiantes;
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO-> consultarTodos());
        $estudiantes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $e = new Proyecto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4],$resultado[5],$resultado[6],$resultado[7],$resultado[8]);
            array_push($estudiantes, $e);
        }
        $this -> conexion -> cerrar();
        return $estudiantes;
    }
    public function consultarTodos2(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO-> consultarTodos2());
        $estudiantes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $e = new Proyecto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4],$resultado[5],$resultado[6],$resultado[7],$resultado[8]);
            array_push($estudiantes, $e);
        }
        $this -> conexion -> cerrar();
        return $estudiantes;
    }
    
    public function insertarTutor(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO -> insertarTutor());
        $this -> conexion -> cerrar();
    }
    public function insertarJurado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO -> insertarJurado());
        $this -> conexion -> cerrar();
    }
    
    public function actualizar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO -> actualizar());
        $this -> conexion -> cerrar();
    }
    
    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    public function consultarP(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO-> consultarP());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    public function cambiarEstado() {
        $this -> conexion -> abrir();
        $this->conexion->ejecutar($this->proyectoDAO -> cambiarEstado());
        $this -> conexion -> cerrar();
    }
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO -> consultarPaginacion($cantidad, $pagina));
        $proyectos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            //$idProyecto= "", $titulo= "", $descripcion= "", $pdf= "",$estudiante= "",$estudiante2= "", $tutor= "",$juez= "",$estado= ""
            $p = new Proyecto($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4],$resultado[5],$resultado[6],$resultado[7],$resultado[8]);
            array_push($proyectos, $p);
        }
        $this -> conexion -> cerrar();
        return $proyectos;
    }
    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO -> consultarFiltro($filtro));
        $proyectos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            //$idProyecto= "", $titulo= "", $descripcion= "", $pdf= "",$estudiante= "",$estudiante2= "", $tutor= "",$juez= "",$estado= ""
            $p = new Proyecto($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4],$resultado[5],$resultado[6],$resultado[7],$resultado[8]);
            array_push($proyectos, $p);
        }
        $this -> conexion -> cerrar();
        return $proyectos;
    }
}


?>