<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/LogAdminDAO.php";
class LogAdmin{
    private $id;
    private $accion;
    private $datos;
    private $fecha;
    private $hora;
    private $administrador;
    private $conexion;
    private $logAdminDAO;

    public function getId()
    {
        return $this->id;
    }
    public function getAccion()
    {
        return $this->accion;
    }

    public function getDatos()
    {
        return $this->datos;
    }
 
    public function getFecha()
    {
        return $this->fecha;
    }

    public function getHora()
    {
        return $this->hora;
    }

    public function getAdministrador()
    {
        return $this->administrador;
    }
    
    public function LogAdmin($id = "", $accion= "", $datos = "", $fecha= "", $hora= "",$administrador=""){
        $this -> id = $id;
        $this -> accion = $accion;
        $this -> datos = $datos;
        $this -> fecha = $fecha;
        $this -> hora = $hora;
        $this -> administrador= $administrador;
        $this -> conexion = new Conexion();
        $this -> logAdminDAO= new LogAdminDAO($this -> id, $this -> accion, $this -> datos, $this -> fecha, $this -> hora,$this -> administrador);
    }

     public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logAdminDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> accion = $resultado[0];
        $this -> datos = $resultado[1];
        $this -> fecha = $resultado[2];
        $this -> hora = $resultado[3];
        $this -> administrador= $resultado[4];
        
    }
    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logAdminDAO -> insertar());
        $this -> conexion -> cerrar();
    }    
    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logAdminDAO -> consultarFiltro($filtro));
        $asignadores = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new LogAdmin($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4],$resultado[5]);
            array_push($asignadores, $p);
        }
        $this -> conexion -> cerrar();
        return $asignadores;
    }
    
}

?>