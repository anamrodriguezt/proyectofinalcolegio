<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/LogProfesorDAO.php";
class LogProfesor{
    private $id;
    private $accion;
    private $datos;
    private $fecha;
    private $hora;
    private $actor;
    private $conexion;
    private $logProfesorDAO;

    public function getId()
    {
        return $this->id;
    }
    public function getAccion()
    {
        return $this->accion;
    }

    public function getDatos()
    {
        return $this->datos;
    }
 
    public function getFecha()
    {
        return $this->fecha;
    }

    public function getHora()
    {
        return $this->hora;
    }
    
    public function getJuez()
    {
        return $this->actor;
    }
    
    public function LogProfesor($id = "", $accion= "", $datos = "", $fecha= "", $hora= "", $juez= "" ){
        $this -> id = $id;
        $this -> accion = $accion;
        $this -> datos = $datos;
        $this -> fecha = $fecha;
        $this -> hora = $hora;
        $this -> actor= $juez;
        $this -> conexion = new Conexion();
        $this -> logProfesorDAO= new LogProfesorDAO($this -> id, $this -> accion, $this -> datos, $this -> fecha, $this -> hora, $this -> actor);
    }

     public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logProfesorDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> accion = $resultado[0];
        $this -> datos = $resultado[1];
        $this -> fecha = $resultado[2];
        $this -> hora = $resultado[3];
        $this -> actor = $resultado[4];
        
     }
     public function insertar(){
         $this -> conexion -> abrir();
         $this -> conexion -> ejecutar($this -> logProfesorDAO -> insertar());
         $this -> conexion -> cerrar();
     }
    
     public function consultarFiltro($filtro){
         $this -> conexion -> abrir();
         $this -> conexion -> ejecutar($this -> logProfesorDAO -> consultarFiltro($filtro));
         $asignadores = array();
         while(($resultado = $this -> conexion -> extraer()) != null){
             $p = new LogProfesor($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4],$resultado[5]);
             array_push($asignadores, $p);
         }
         $this -> conexion -> cerrar();
         return $asignadores;
     }
     public function consultarLog(){
         $this -> conexion -> abrir();
         $this -> conexion -> ejecutar($this -> logProfesorDAO -> consultarLog());
         $asignadores = array();
         while(($resultado = $this -> conexion -> extraer()) != null){
             $p = new LogProfesor($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4],$resultado[5]);
             array_push($asignadores, $p);
         }
         $this -> conexion -> cerrar();
         return $asignadores;
     }
}

?>