<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/LogAsignadorDAO.php";
class LogAsignador{
    private $id;
    private $accion;
    private $datos;
    private $fecha;
    private $hora;
    private $asignador;
    private $conexion;
    private $logAsignadorDAO;

    public function getId()
    {
        return $this->id;
    }
    public function getAccion()
    {
        return $this->accion;
    }

    public function getDatos()
    {
        return $this->datos;
    }
 
    public function getFecha()
    {
        return $this->fecha;
    }

    public function getHora()
    {
        return $this->hora;
    }
    
    public function getAsignador()
    {
        return $this->asignador;
    }
    
    public function LogAsignador($id = "", $accion= "", $datos = "", $fecha= "", $hora= "", $asignador= "" ){
        $this -> id = $id;
        $this -> accion = $accion;
        $this -> datos = $datos;
        $this -> fecha = $fecha;
        $this -> hora = $hora;
        $this -> asignador= $asignador;
        $this -> conexion = new Conexion();
        $this -> logAsignadorDAO= new LogAsignadorDAO($this -> id, $this -> accion, $this -> datos, $this -> fecha, $this -> hora, $this -> asignador);
    }

     public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logAsignadorDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> accion = $resultado[0];
        $this -> datos = $resultado[1];
        $this -> fecha = $resultado[2];
        $this -> hora = $resultado[3];
        $this -> asignador = $resultado[4];
        
     }
     public function insertar(){
         $this -> conexion -> abrir();
         $this -> conexion -> ejecutar($this -> logAsignadorDAO -> insertar());
         $this -> conexion -> cerrar();
     }
     public function consultarFiltro($filtro){
         $this -> conexion -> abrir();
         $this -> conexion -> ejecutar($this -> logAsignadorDAO -> consultarFiltro($filtro));
         $asignadores = array();
         while(($resultado = $this -> conexion -> extraer()) != null){
             $p = new LogAsignador($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4],$resultado[5]);
             array_push($asignadores, $p);
         }
         $this -> conexion -> cerrar();
         return $asignadores;
     }
     public function consultarLog(){
         $this -> conexion -> abrir();
         $this -> conexion -> ejecutar($this -> logAsignadorDAO -> consultarLog());
         $asignadores = array();
         while(($resultado = $this -> conexion -> extraer()) != null){
             $p = new LogAsignador($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4],$resultado[5]);
             array_push($asignadores, $p);
         }
         $this -> conexion -> cerrar();
         return $asignadores;
     }
    
    
    
}

?>