<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ProcesoDAO.php";
class Proceso{
    private $idProceso;
    private $descripcion;
    private $autor;
    private $rol;
    private $fecha;
    private $hora;
    private $proyecto;
    private $conexion;
    private $procesoDAO;

    
    
    /**
     * @return mixed
     */
    public function getIdProceso()
    {
        return $this->idProceso;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @return mixed
     */
    public function getAutor()
    {
        return $this->autor;
    }

    /**
     * @return mixed
     */
    public function getRol()
    {
        return $this->rol;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @return mixed
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * @return mixed
     */
    public function getProyecto()
    {
        return $this->proyecto;
    }

    public function Proceso($idProceso= "", $descripcion= "", $autor= "", $rol= "", $fecha = "",$hora = "",$proyecto= ""){
        $this -> idProceso= $idProceso;
        $this -> descripcion = $descripcion;
        $this -> autor = $autor;
        $this -> rol = $rol;
        $this -> fecha = $fecha;
        $this -> hora = $hora;
        $this -> proyecto = $proyecto;
        $this -> conexion = new Conexion();
        $this -> procesoDAO = new ProcesoDAO($this -> idProceso, $this -> descripcion, $this -> autor, $this -> rol, $this -> fecha,$this -> hora,$this -> proyecto);
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> procesoDAO-> consultarTodos());
        $estudiantes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $e = new Proceso($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4],$resultado[5],$resultado[6]);
            array_push($estudiantes, $e);
        }
        $this -> conexion -> cerrar();
        return $estudiantes;
    }
    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> procesoDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    

    
}

?>