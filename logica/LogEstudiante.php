<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/LogEstudianteDAO.php";
class LogEstudiante{
    private $id;
    private $accion;
    private $datos;
    private $fecha;
    private $hora;
    private $estudiante;
    private $conexion;
    private $logEstudianteDAO;

    public function getId()
    {
        return $this->id;
    }
    public function getAccion()
    {
        return $this->accion;
    }

    public function getDatos()
    {
        return $this->datos;
    }
 
    public function getFecha()
    {
        return $this->fecha;
    }

    public function getHora()
    {
        return $this->hora;
    }
    
    public function getEstudiante()
    {
        return $this->estudiante;
    }
    
    public function LogEstudiante($id = "", $accion= "", $datos = "", $fecha= "", $hora= "", $estudiante= "" ){
        $this -> id = $id;
        $this -> accion = $accion;
        $this -> datos = $datos;
        $this -> fecha = $fecha;
        $this -> hora = $hora;
        $this -> estudiante= $estudiante;
        $this -> conexion = new Conexion();
        $this -> logEstudianteDAO= new LogEstudianteDAO($this -> id, $this -> accion, $this -> datos, $this -> fecha, $this -> hora, $this -> estudiante);
    }
    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logEstudianteDAO -> insertar());
        $this -> conexion -> cerrar();
    }

     public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logEstudianteDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> accion = $resultado[0];
        $this -> datos = $resultado[1];
        $this -> fecha = $resultado[2];
        $this -> hora = $resultado[3];
        $this -> estudiante = $resultado[4];
        
     }
    
     public function consultarFiltro($filtro){
         $this -> conexion -> abrir();
         $this -> conexion -> ejecutar($this -> logEstudianteDAO -> consultarFiltro($filtro));
         $asignadores = array();
         while(($resultado = $this -> conexion -> extraer()) != null){
             $p = new LogEstudiante($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4],$resultado[5]);
             array_push($asignadores, $p);
         }
         $this -> conexion -> cerrar();
         return $asignadores;
     }
     public function consultarLog(){
         $this -> conexion -> abrir();
         $this -> conexion -> ejecutar($this -> logEstudianteDAO -> consultarLog());
         $asignadores = array();
         while(($resultado = $this -> conexion -> extraer()) != null){
             $p = new LogEstudiante($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4],$resultado[5]);
             array_push($asignadores, $p);
         }
         $this -> conexion -> cerrar();
         return $asignadores;
     }
    
}

?>