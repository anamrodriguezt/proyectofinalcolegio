<?php
$proyecto = new Profesor($_SESSION["id"]);
$proyecto -> consultar();

?>	
<div class="container mt-3">	
	<div class="row">
	<div class="col-lg-3 col-md-0"></div>
   <div class="col-lg-6 col-md-12">
    <div class="card">
      <div class="p-3 mb-2 bg-info text-white">
       <h4 class="text-center">Bienvenido</h4> 
      </div>
      <div class="card-body">
        <table class="table table-hover">
			<tr>
				<th>Nombre</th>
				<td><?php echo $proyecto-> getNombre() ?></td>
			</tr>
			<tr>
				<th>Apellido</th>
				<td><?php echo $proyecto-> getApellido() ?></td>
			</tr>
			<tr>
				<th>Correo</th>
				<td><?php echo $proyecto-> getCorreo() ?></td>
			</tr>
		</table>
						
      </div>
    </div>
    </div>
   </div>
   
</div>		