<?php 
$administrador = new Administrador($_SESSION["id"]);
$administrador -> consultar();
$nombre = $administrador -> getNombre();
$apellido = $administrador -> getApellido();
$correo = $administrador -> getCorreo();
?>
<nav class="navbar navbar-expand-lg navbar-light bg-dark">
  <a class="navbar-brand"
		href="index.php?pid=<?php echo base64_encode("presentacion/sesionAdministrador.php") ?>"><i
		class="fas fa-home"></i></a>
  <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon text-white"></span> 
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         Proyectos
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php  echo base64_encode("presentacion/administrador/consultarProyectos.php")?>">Consultar</a>
          <a class="dropdown-item" href="index.php?pid=<?php  echo base64_encode("presentacion/administrador/buscarProyectos.php")?>"">Buscar</a>
        </div>
      </li>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Estudiantes
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/buscarEstudiantes.php")?>">Buscar</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/consultarEstudiantes.php")?>">Consultar</a>
        </div>
      </li>     
     <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Asignadores
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/buscarAsignadores.php")?>">Buscar</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/consultarAsignadores.php")?>">Consultar</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/registrarAsignador.php")?>">Crear</a>
        </div>
      </li>     
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Profesores
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/buscarProfesores.php")?>">Buscar</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/consultarProfesor.php")?>">Consultar</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/registrarProfesor.php")?>">Crear</a>
        </div>
      </li>     
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Log
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/buscarLogAdmin.php")?>">Administrador</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/buscarLogEstudiante.php")?>">Estudiante</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/buscarLogAsignador.php")?>">Asignador</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/buscarLogProfesor.php")?>">Profesor</a>
          
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Estadisticas
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/proyectosAprobados.php")?>">Proyectos</a>
          
        </div>
      </li>
    </ul>
    
    
    <span class="navbar-text text-white">
      Administrador:<?php echo $nombre!="" && $apellido!=""?" " . $nombre. " ". $apellido :  $correo ?>
    </span>
    <ul class="navbar-nav">
    <li class="nav-item active "><a class="nav-link text-white"
				href="index.php?cerrarSesion=true">Cerrar Sesion</a></li>
    </ul>
  </div>
</nav>