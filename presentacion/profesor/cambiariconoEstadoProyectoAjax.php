<?php
$idProyecto = $_GET["idProyecto"];
$nuevoEstado = $_GET["nuevoEstado"];
$idProfesor=$_GET["idProfesor"];
$idProfesor=$_GET["idEstudiante"];
$proyecto = new Proyecto($idProyecto,"","","",$idProfesor);
$proyecto->consultar2();
$profesor=new Profesor($_GET["idProfesor"]);
$profesor->consultar();

echo "<a id='cambiarEstado" . $proyecto-> getIdProyecto() . "' href='#' >" . (($proyecto -> getEstado()==1)?"<span class='fas fa-check-circle' data-toggle='tooltip' data-placement='left' title='Aprobar'></span>":(($proyecto-> getEstado()==0)?"<span class='fas fa-times-circle' data-toggle='tooltip' data-placement='left' title='Desaprobar'></span>":"")) . "</a>";
?>

<script>
$(document).ready(function(){
	$("#cambiarEstado<?php echo $idProyecto?>").click(function(e){
		$('[data-toggle="tooltip"]').tooltip('hide');
		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/profesor/cambiarEstadoProyectoAjax.php") ?>&idProfesor=<?php echo $idProfesor?>&idProyecto=<?php echo $idProyecto?>&idEstudiante=<?php echo $idProfesor  ?>&nuevoEstado=<?php echo (($nuevoEstado==1)?"0":"1")?>";		
		$("#estado<?php echo $idProyecto?>").load(url);
		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/profesor/cambiarIconoEstadoProyectoAjax.php") ?>&idProfesor=<?php echo $idProfesor?>&idProyecto=<?php echo $idProyecto?>&idEstudiante=<?php echo $idProfesor  ?>&nuevoEstado=<?php echo (($nuevoEstado==1)?"0":"1")?>";
		$("#accion<?php echo $idProyecto?>").load(url);
	});		
});
</script>