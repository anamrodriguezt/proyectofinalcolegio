<?php
$profesor= new Profesor($_SESSION['id']);
$profesor->consultar();
//include 'presentacion/menuAdministrador.php';
$error="";
$idproyecto= $_GET["idProyecto"] ;
$proyecto=new Proyecto($idproyecto);
$proyecto -> consultar();
if(isset($_POST['agregar'])){
    $texto = $_POST['texto'];
    $comentario= new Comentario("", $texto,$idproyecto,date("Y-m-d H:m:s"));
    $comentario->insertar();
    $descripcion= "El tutor ". $profesor -> getNombre()." ".$profesor -> getApellido()." agrego un comentario en el proyecto (".$proyecto ->getTitulo().")";
    $proceso = new Proceso("",$descripcion,$_SESSION["id"],$_SESSION["rol"],date('Y-m-d'),date('H:i:s'),$idproyecto);
    $proceso -> insertar();
    
    $accion= "Agrego comentario a: ".$proyecto->getTitulo();
    $datos = "Asignador:".$profesor->getNombre()." ".$profesor->getApellido();
    $log = new LogProfesor("",$accion, $datos, date('Y-m-d'), date('H:i:s'),$_SESSION["id"]);
    $log->insertar();
    
        $error=0;
    }else{
        $error=1;
    }


?>
<br></br>
<div class="row">
  <div class="col-s-12">
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-sm-4">
    </div>
    <div class="col-sm-4">
      <div class="card">
        <div class="card-body bg-warning" >
          <h5 class="h5 text-dark  text-center">A&ntilde;adir comentarios</h5>
        </div>
      </div>
      <div class="card">
        <div class="card-body ">

          <?php if (isset($_POST['agregar'])) { ?>
					<div class="alert alert-<?php echo ($error == 0) ? "success" : "danger" ?> alert-dismissible fade show" role="alert">
						<?php echo ($error == 0) ? "Comentario agregado exitosamente" : "No se pudo agregar"; ?>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<?php } ?>

			<form method="post" action="index.php?pid=<?php echo base64_encode("presentacion/profesor/agregarComentario.php") ?>>&idProyecto=<?php echo $idproyecto?>">
			<div class='form-group'>
			<center><textarea  style="width:300px ;height:100px" name="texto" placeholder="Comentarios" ></textarea><p>
		      <center><input type="submit" name="agregar"  class="btn btn-outline-dark" value="A&ntilde;adir">
            </div>	
            </form>
        </div>
      </div>
      <br>
    </div>
  </div>
</div>
