<?php
$profesor= new Profesor($_SESSION["id"]);
$profesor->consultar();
$proyecto = new Proyecto("","","","","","",$_SESSION["id"]);
$proyectos = $proyecto->consultarPT();

?>
	<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Proyectos donde eres Tutor</h4>
				</div>
				<div class="col">
					<div class="text-left"><?php echo count($proyectos) ?> registros encontrados</div>
				</div>
              	<div class="card-body">
              	<?php if(count($proyectos)!=0){?>
					<table class="table table-hover table-striped table-responsive-lg">
						<tr>
							<th>#</th>
							<th>Titulo</th>
							<th>Descripcion</th>
							<th>PDF</th>
							<th>Autores</th>
							<th>Jurado</th>
							<th>Estado</th>
							<th>Servicios</th>
							
							
						</tr>
						<?php 
						$i=1;
						foreach($proyectos as $p){
						    
						    $estudiante = new Estudiante($p->getEstudiante());
						    $estudiante->consultar();
						    if($p -> getEstudiante2()!=""){
						        $estudiante2 = new Estudiante($p->getEstudiante2());
						        $estudiante2->consultar();
						    }
						    $profesor= new Profesor($p->getJurado());
						    $profesor->consultar();
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $p -> getTitulo() . "</td>";
						    echo "<td>" . $p -> getDescripcion() . "</td>";
						    echo "<td><center><a class='fas fa-file-pdf'  href='index.php?pid=" . base64_encode("presentacion/estudiante/verPDF.php") ."&idProyecto=" . $p->getIdProyecto() . "' data-toggle='tooltip' data-placement='left' title='ver PDF'></a>" . "</td>";
						    echo "";
						    echo "<td>" . $estudiante->getNombre()." ". $estudiante->getApellido().(($p->getEstudiante2()=="")?"":", ".$estudiante2->getNombre()." ". $estudiante2->getApellido())."</td>";
						    echo "<td>" . (($p -> getJurado()=="")?"Sin asignar":$profesor->getNombre()." ".$profesor->getApellido()) . "</td>";
						    echo "<td>" . (($p-> getEstado()==1)?"<div id='estado". $p->getIdProyecto() ."'>Aprobado</div>":"<div id='estado". $p->getIdProyecto() ."'>Sin aprobar</div>")."</td>";
						    echo "</div></td>";
						    echo "<td><a class='fas fa-edit' href='index.php?pid=" . base64_encode("presentacion/profesor/agregarComentario.php") . "&idProyecto=". $p->getIdProyecto() ."' data-toggle='tooltip' data-placement='left' title='agregar comentario'> </a>";
						    echo "<a href='modaldetalles.php?idProyecto=" . $p->getIdProyecto() . "' data-toggle='modal' data-target='#modalProyecto' ><span class='fas fa-eye' data-toggle='tooltip' class='tooltipLink' data-placement='left' data-original-title='Comentarios' ></span> </a>
						          <a class='fas fa-file-pdf' target='_blank' href='procesoPDF.php?idProyecto=". $p-> getIdProyecto() . "' data-toggle='tooltip' data-placement='left' title='Reporte del proceso'> </a>" . "</td>";
						    echo "</td>";
						    echo "</tr>";
						     $i++;
						}
						?>
					</table>
					<?php }else{?>
					    <div class="alert alert-danger alert-dismissible fade show" role="alert">
					    No eres tutor de ningun proyecto.
					    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					    </div>
					<?php }?>
				</div>
            </div>
		</div>
	</div>
</div>



<div class="modal fade" id="modalProyecto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>