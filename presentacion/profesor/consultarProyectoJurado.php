<?php
$proyecto= new Profesor($_SESSION["id"]);
$proyecto->consultar();
$proyecto = new Proyecto("","","","","","","",$_SESSION["id"]);
$proyectos = $proyecto->consultarPJ();
?>
	<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Proyectos donde eres Jurado</h4>
				</div>
				<div class="col">
					<div class="text-left"><?php echo count($proyectos) ?> registros encontrados</div>
				</div>
              	<div class="card-body">
              		<?php if(count($proyectos)!=0){?>
					<table class="table table-hover table-striped table-responsive-lg">
						<tr>
							<th>#</th>
							<th>Titulo</th>
							<th>Descripcion</th>
							<th>PDF</th>
							<th>Autores</th>
							<th>Tutor</th>
							<th>Estado</th>
							<th>Servicios</th>
							
							
						</tr>
						<?php 
						$i=1;
						foreach($proyectos as $p){
						    
						    $estudiante = new Estudiante($p->getEstudiante());
						    $estudiante->consultar();
						    if($p -> getEstudiante2()!=""){
						        $estudiante2 = new Estudiante($p->getEstudiante2());
						        $estudiante2->consultar();
						    }
						    $profesor= new Profesor($p->getTutor());
						    $profesor->consultar();
						    
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $p -> getTitulo() . "</td>";
						    echo "<td>" . $p -> getDescripcion() . "</td>";
						    echo "<td><center><a class='fas fa-file-pdf' href='index.php?pid=" . base64_encode("presentacion/estudiante/verPDF.php") . "&idProyecto=" . $p->getIdProyecto() . "' data-toggle='tooltip' data-placement='left' title='ver PDF'> </a>" . "</td>";
						    echo "";
						    echo "<td>" . $estudiante->getNombre()." ". $estudiante->getApellido().(($p->getEstudiante2()=="")?"":", ".$estudiante2->getNombre()." ". $estudiante2->getApellido())."</td>";
						    echo "<td>" . (($p -> getTutor()=="")?"Sin asignar":$profesor->getNombre()." ".$profesor->getApellido()) . "</td>";

						    echo "<td>" . (($p-> getEstado()==1)?"<div id='estado". $p->getIdProyecto() ."'>Aprobado</div>":"<div id='estado". $p->getIdProyecto() ."'>Sin aprobar</div>")."</td>";
						    echo "<td><div id='accion" . $p -> getIdProyecto() . "'><a id='cambiarEstado" . $p-> getIdProyecto() . "' href='#' >" . (($p -> getEstado()==1)?"<span class='fas fa-check-circle' data-toggle='tooltip' data-placement='left' title='Desaprobar'></span>":(($p-> getEstado()==0)?"<span class='fas fa-times-circle' data-toggle='tooltip' data-placement='left' title='Aprobar'></span>":"")) . "</a></div>
						              <a class='fas fa-file-pdf' target='_blank' href='procesoPDF.php?idProyecto=". $p-> getIdProyecto() . "' data-toggle='tooltip' data-placement='left' title='Reporte del proceso'> </a>" . "</td>";
						    ?>
					     <script>
                        $(document).ready(function(){
                        	$("#cambiarEstado<?php echo $p-> getIdProyecto() ?>").click(function(e){
                        		$('[data-toggle="tooltip"]').tooltip('hide');
                        		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/profesor/cambiarEstadoProyectoAjax.php") ?>&idProyecto=<?php echo $p -> getIdProyecto() ?>&idProfesor=<?php echo $_SESSION["id"]  ?>&idEstudiante=<?php echo $p->getEstudiante()  ?>&nuevoEstado=<?php echo (($p -> getEstado()==1)?"0":"1")?>";		
                        		$("#estado<?php echo $p-> getIdProyecto() ?>").load(url);
                        		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/profesor/cambiarIconoEstadoProyectoAjax.php") ?>&idProyecto=<?php echo $p-> getIdProyecto() ?>&idProfesor=<?php echo $_SESSION["id"] ?>&idEstudiante=<?php echo $p->getEstudiante()  ?>&nuevoEstado=<?php echo (($p -> getEstado()==1)?"0":"1")?>";
                        		$("#accion<?php echo $p-> getIdProyecto() ?>").load(url);
                        	});
                        });
                        
                        </script>
						
						<?php   						
						echo "</div></td>";
						
						    
						    echo "</tr>";
						     $i++;
						}
						?>
					</table>
					<?php }else{?>
					    <div class="alert alert-danger alert-dismissible fade show" role="alert">
					    No eres jurado de ningun proyecto.
					    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					    </div>
					<?php }?>
				</div>
            </div>
		</div>
	</div>
</div>
