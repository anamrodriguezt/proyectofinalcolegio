<?php 
$proyecto = new Estudiante($_SESSION["id"]);
$proyecto -> consultar();
$nombre = $proyecto-> getNombre();
$apellido = $proyecto -> getApellido();
$correo = $proyecto -> getCorreo();
?>
<nav class="navbar navbar-expand-lg navbar-light bg-dark">
  <a class="navbar-brand"
		href="index.php?pid=<?php echo base64_encode("presentacion/sesionEstudiante.php") ?>"><i
		class="fas fa-home "></i></a>
  <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon text-white"></span> 
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
  	<li class="nav-item dropdown"><a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         Proyecto
        </a><div class="dropdown-menu" aria-labelledby="navbarDropdown">
			<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/estudiante/hacerProyecto.php")?>">Crear</a>
			<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/estudiante/actualizarProyecto.php")?>">Actualizar</a>
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/estudiante/consultarProyecto.php")?>">Consultar</a>
				</div></li>
			
	  <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Log
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/estudiante/consultarLogEstudiante.php")?>">Consultar</a>
          
        </div>
      </li>
    </ul>
    <span class="navbar-text text-white">
      Estudiante:<?php echo $nombre!="" && $apellido!=""?" " . $nombre. " ". $apellido :  $correo ?>
    </span>
    <ul class="navbar-nav">
    <li class="nav-item active "><a class="nav-link text-white"
				href="index.php?cerrarSesion=true">Cerrar Sesion</a></li>
    </ul>
  </div>
</nav>