<div class="container mt-3">
	<div class="row">
		<div class="col-lg-9 col-md-8 col-12">
            <div class="card">
				<div class="p-3 mb-2 bg-info text-white">
					<h4>Proyectos de grado Colegio Distrital Apli</h4>
				</div>
              	<div class="card-body">
              		<img src="img/portada.jpg" width="100%">
            	</div>
            </div>
		</div>
		<div class="col-lg-3 col-md-4 col-12 text-center">
            <div class="card">
				<div class="p-3 mb-2 bg-info text-white">
					<h4>Autenticacion</h4>
				</div>
              	<div class="card-body">
        			<form action="index.php?pid=<?php echo base64_encode("presentacion/autenticar.php") ?>" method="post">
        				<div class="form-group">
    						<input name="correo" type="email" class="form-control" placeholder="Correo" required>
    					</div>
        				<div class="form-group">
    						<input name="clave" type="password" class="form-control" placeholder="Clave" required>
    					</div>
        				<div class="form-group">
    						<input name="ingresar" type="submit" class="form-control btn btn-p-3 mb-2 bg-info text-white">
    					</div>
    					<?php 
    					if(isset($_GET["error"]) && $_GET["error"]==1){
    					    echo "<div class=\"alert alert-danger\" role=\"alert\">Error de correo o clave</div>";
    					}else if(isset($_GET["error"]) && $_GET["error"]==2){
    					    echo "<div class=\"alert alert-danger\" role=\"alert\">Su cuenta no ha sido activada</div>";    					   
    					}else if(isset($_GET["error"]) && $_GET["error"]==3){
    					    echo "<div class=\"alert alert-danger\" role=\"alert\">Su cuenta ha sido inhabilitada</div>";
    					}
    					?>
        			</form>
        							<a  class="font-italic" href="index.php?pid=<?php echo base64_encode("presentacion/estudiante/registrarEstudiante.php")?>">Registrar Estudiante</a> 					
    					
        			</div>
            </div>
		</div>
	</div>
</div>
