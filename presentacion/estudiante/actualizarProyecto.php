<?php
$proyecto= new Proyecto("","","","", $_SESSION['id']);
$proyectos = $proyecto-> consultarP();
$error=-1;
if($proyectos!=0){
$proyecto-> consultar2(); 
$idproyecto=$proyecto->getIdProyecto();
if(isset($_POST["editar"])){
            if ($_FILES["pdf"]["name"]!=""){
            if($_FILES["pdf"]["type"] == "application/pdf"){
                $rutaLocal = $_FILES["pdf"]["tmp_name"];
                $tipo = $_FILES["pdf"]["type"];
                $tiempo = new DateTime();
                $rutaRemota = "pdfs/" . $tiempo -> getTimestamp() .".pdf";
                copy($rutaLocal,$rutaRemota);
                $descripcion= "se actualizo el proyecto (".$_POST["titulo"].")";
                $proyecto = new Proyecto("", $_POST["titulo"], $_POST["descripcion"], $rutaRemota, $_SESSION["id"]);
                $proyecto-> actualizar();
                $proyecto -> consultar2();
                $proceso = new Proceso("",$descripcion,$_SESSION["id"],$_SESSION["rol"],date('Y-m-d'),date('H:i:s'),$proyecto -> getIdProyecto());
                $proceso -> insertar();
                $estudiante = new Estudiante($_SESSION["id"]);
                $estudiante->consultar();
                $accion= "actializo el proyecto proyecto ".$proyecto->getTitulo();
                $datos = "Estudiante:".$estudiante->getNombre()." ".$estudiante->getApellido();
                $log = new LogEstudiante("",$accion, $datos, date('Y-m-d'), date('H:i:s'),$_SESSION["id"]);
                $log->insertar();
                $error=0;
            }else{
                $error=1;
            }
            }else{
                $pdf=$proyecto->getPdf();
                $proyecto = new Proyecto("", $_POST["titulo"], $_POST["descripcion"],$pdf, $_SESSION["id"]);
                $proyecto-> actualizar();
                $proyecto -> consultar2();
                $descripcion= "se actualizo el proyecto (".$_POST["titulo"].")";
                $proceso = new Proceso("",$descripcion,$_SESSION["id"],$_SESSION["rol"],date('Y-m-d'),date('H:i:s'),$proyecto -> getIdProyecto());
                $proceso -> insertar();
                $estudiante = new Estudiante($_SESSION["id"]);
                $estudiante->consultar();
                $accion= "actializo el proyecto proyecto ".$proyecto->getTitulo();
                $datos = "Estudiante:".$estudiante->getNombre()." ".$estudiante->getApellido();
                $log = new LogEstudiante("",$accion, $datos, date('Y-m-d'), date('H:i:s'),$_SESSION["id"]);
                $log->insertar();
                $error=0;
            }
               }
}else{
    $error=2;
}
//         $actor=$_SESSION["rol"].":"." ". $proveedor->getNombre(). " " .$proveedor->getApellido();
//         $log = new Log("", $accion, $dato, date('Y-m-d'), date('H:i:s'),$actor );
//         $log->insertar();
       
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-black bg-warning">
					<h4>Editar Proyecto</h4>
				</div>
              	<div class="card-body">
					   <?php if($error==0){?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos editados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } if($error==1){?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						El archivo debe ser PDF
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php }
					if($error==2){?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						No se ha creado un proyecto
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php }
					if($proyectos!=0){
					?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/estudiante/actualizarProyecto.php") ?>&idProyecto=<?php echo $idproyecto?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Titulo</label> 
							<input type="text" name="titulo" class="form-control" value="<?php echo $proyecto-> getTitulo() ?>" required>
						</div>
						<div class="form-group">
							<label>Descripcion</label> 
							<input type="text" name="descripcion" class="form-control" value="<?php echo $proyecto-> getDescripcion() ?>" required>
						</div>
						
						<div class="form-group">
							<label>Pdf</label> 
							<input type="file" name="pdf" class="form-control" >
						</div>
						
						<button type="submit" name="editar" class="btn btn-warning">Editar</button>
					</form>
					<?php }else{
					    $error = 2;
					}?>
            	</div>
            </div>
		</div>
	</div>
</div>