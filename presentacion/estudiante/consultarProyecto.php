<?php
$estudiante= new Estudiante($_SESSION["id"]);
$proyecto = new Proyecto("","","","",$_SESSION["id"]);
$estudiante->consultar();
$error=0;

       


?>
	<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-black bg-warning">
					<h4>Consultar proyecto: <?php echo  " " .$estudiante->getNombre(). " " .$estudiante->getApellido()?> </h4>
				</div>
					<div class="card-body">						
					<?php if($proyecto -> consultar3()==1){
                    $proyecto->consultar2();
                    ?>
    					<table class="table table-hover table-striped table-responsive-lg">
    						<tr>							
    							<th>Titulo</th>
    							<th>Descripcion</th>
    							<th>PDF</th>
    							<th>Autores</th>
    							<th>Tutor</th>
    							<th>Juez</th>
    							<th>Estado</th>
    							<th>Servicios</th>
    						</tr>
    						<?php 
    			
    						    
    						    $estudiante = new Estudiante($proyecto->getEstudiante());
    						    $estudiante->consultar();
    						    if($proyecto -> getEstudiante2()!=""){
    						        $estudiante2 = new Estudiante($proyecto->getEstudiante2());
    						    $estudiante2->consultar();
    						    }
    						   
    						    if($proyecto->getTutor()!=""){
    						        $profesor = new Profesor($proyecto->getTutor());
    						        $profesor->consultar();
    						    }
    						    if($proyecto -> getJurado()!=""){
    						        $profesor2 = new Profesor($proyecto->getjurado());
    						        $profesor2->consultar();
    						        
    						    }
    						    
    						    echo "<tr>";
    						    echo "<td>" . $proyecto -> getTitulo() . "</td>";
    						    echo "<td>" . $proyecto -> getDescripcion() . "</td>";
    						    echo "<td><center><a class='fas fa-file-pdf' href='index.php?pid=" . base64_encode("presentacion/estudiante/verPDF.php") . "&idProyecto=" . $proyecto->getIdProyecto() . "' data-toggle='tooltip' data-placement='left' title='ver PDF'> </a>" . "</td>";
    						    echo "<td>" . $estudiante->getNombre()." ". $estudiante->getApellido().(($proyecto->getEstudiante2()=="")?"":", ".$estudiante2->getNombre()." ". $estudiante2->getApellido())."</td>";
    						    echo "<td>" . (($proyecto -> getTutor()=="")?"Sin asignar":$profesor->getNombre()." ".$profesor->getApellido()) . "</td>";
    						    echo "<td>" . (($proyecto -> getJurado()=="")?"Sin asignar":$profesor2->getNombre()." ".$profesor2->getApellido()) . "</td>";
    						    echo "<td>" . (($proyecto -> getEstado()==0)?"No aprobado":"Aprobado"). "</td>";
    						    echo "<td><a href='modaldetalles.php?idProyecto=" . $proyecto->getIdProyecto() . "' data-toggle='modal' data-target='#modalProyecto' ><span class='fas fa-eye' data-toggle='tooltip' class='tooltipLink' data-placement='left' data-original-title='Comentarios' ></span> </a>
                                            <a class='fas fa-file-pdf' target='_blank' href='procesoPDF.php?idProyecto=". $proyecto-> getIdProyecto() . "' data-toggle='tooltip' data-placement='left' title='Reporte del proceso'> </a>" . "</td>";
    						    echo "</tr>";
    
    						?>
    					</table>
    					<?php }else{
    					$error=1;
    					}?>
    					<?php if($error == 1){ ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						Todavia no has creado un proyecto.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php }?>
    				</div>
				</div>
            </div>
		</div>
	</div>


<div class="modal fade" id="modalProyecto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	<div class="modal-dialog modal-lg" >

		<div class="modal-content" id="modalContent">

		</div>

	</div>

</div>

<script>

	$('body').on('show.bs.modal', '.modal', function (e) {

		var link = $(e.relatedTarget);

		$(this).find(".modal-content").load(link.attr("href"));

	});

</script>