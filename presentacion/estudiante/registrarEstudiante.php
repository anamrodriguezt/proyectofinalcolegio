<?php
include "presentacion/encabezado.php";

$error = 0;

$registrado = false;
if(isset($_POST["registrar"])){
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $correo = $_POST["correo"];
    $clave = $_POST["clave"];
    $codigo = $_POST["codigo"];
    $estudiante1 = new Estudiante("", $nombre, $apellido, $correo, $clave, $codigo);
    if($estudiante1-> existeCorreo()){
        $error = 1;
    }else{
        $estudiante1-> registrar();
        $registrado = true;
        
        $estudiante = new Estudiante("","","",$_POST["correo"]);
        $estudiante->consultar1();
        $e=$estudiante->getIdEstudiante();
        $accion="Registro";
        $datos = "Estudiante";
        $log = new LogEstudiante("",$accion, $datos, date('Y-m-d'), date('H:i:s'),$e);
        $log->insertar();
        
    }
    
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-black bg-warning">
					<h4>Registro Estudiante</h4>
				</div>
              	<div class="card-body">
        			<?php if($error == 1){ ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						El correo <?php echo $correo ?> ya se encuentra registrado.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } else if($registrado) { ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Registro exitoso
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
            	<form action="index.php?pid=<?php echo base64_encode("presentacion/estudiante/registrarEstudiante.php") ?>" method="post">
        				
        				
        				<div class="form-group">
    						<input name="nombre" type="text" class="form-control" placeholder="Nombre" required>
    					</div>
        				<div class="form-group">
    						<input name="apellido" type="text" class="form-control" placeholder="Apellido" required>
    					</div>
        				<div class="form-group">
    						<input name="correo" type="email" class="form-control" placeholder="Correo" required>
    					</div>
        				<div class="form-group">
    						<input name="clave" type="password" class="form-control" placeholder="Clave" required>
    					</div>
        				<div class="form-group">
    						<input name="codigo" type="number" class="form-control" placeholder="Codigo" required>
    					</div>
    	               <center><button type="submit" name="registrar" class="btn btn-warning">Registro estudiantes</button><br></center>
        			    				<a  class="font-italic" href="index.php?pid=<?php echo base64_encode("presentacion/inicio")?>">Volver al Inicio</a> 					
    					</form>     
        		</div>
            </div>		
		</div>
	</div>
</div>
