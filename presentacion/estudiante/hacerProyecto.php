<?php

$proyecto= new Proyecto("","","","", $_SESSION['id']);
$proyectos = $proyecto-> consultarP(); 
if($proyectos==0){
    $error=3;
if(isset($_POST["registrar"])){      
    

        if($_FILES["pdf"]["type"] == "application/pdf"){
        $rutaLocal = $_FILES["pdf"]["tmp_name"];
        $tipo = $_FILES["pdf"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "pdfs/" . $tiempo -> getTimestamp() .".pdf";
        copy($rutaLocal,$rutaRemota);
        $descripcion= "se creo el proyecto (".$_POST["titulo"].")";
        $proyecto = new Proyecto("", $_POST["titulo"], $_POST["descripcion"], $rutaRemota, $_SESSION["id"],$_POST["estudiante"]);
        $proyecto -> insertar();
        $proyecto -> consultar2();
        $proceso = new Proceso("",$descripcion,$_SESSION["id"],$_SESSION["rol"],date('Y-m-d'),date('H:i:s'),$proyecto -> getIdProyecto());
        $proceso -> insertar();
        $error=0;
        
        $estudiante = new Estudiante($_SESSION["id"]);
        $estudiante->consultar();
        $accion= "Creo proyecto ".$_POST["titulo"];
        $datos = "Estudiante:".$estudiante->getNombre()." ".$estudiante->getApellido();
        $log = new LogEstudiante("",$accion, $datos, date('Y-m-d'), date('H:i:s'),$_SESSION["id"]);
        $log->insertar();
        }else{
            $error=2;
        }
}
}else{
    $error=1;
}

?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-black bg-warning">
					<h4>Registro</h4>
				</div>
              	<div class="card-body">
              	<?php if($error == 1){ ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						Ya se ha creado un proyecto anteriormente.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } else if($error==2) { ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						El tipo de archivo debe ser PDF.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } else if($error==0) { ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Registro exitoso
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					
        			<form  enctype="multipart/form-data" action="index.php?pid=<?php echo base64_encode("presentacion/estudiante/hacerProyecto.php") ?>" method="post">
        				<?php if($proyectos==0){?>
        				<div class="form-group">
    						<input name="titulo" type="text" class="form-control" placeholder="Titulo" required>
    					</div>
        				<div class="form-group">
    						<input name="descripcion" type="text" class="form-control" placeholder="Descripcion" required>
    					</div>
        				<div class="form-group">
    						<input  name="pdf" type="file" class="form-control" placeholder="Pdf" required>
    					</div>
        			         <div class="form-group">
							<label>A&ntilde;adir compa&ntilde;ero</label>
							<select class="form-control"
								name="estudiante">
								<?php
								$estudiante = new Estudiante($_SESSION['id']);
								$estudiantes = $estudiante -> consultarTodos();
							    $estudiante -> consultar();
								echo "<option value='". -1 ."'>Ninguno</option>";
								foreach ($estudiantes as $g){
								    if($g-> getIdEstudiante()!= $estudiante -> getIdEstudiante() && $proyecto -> consultar4($g -> getIdEstudiante())==0){
								    echo "<option value='" . $g -> getIdEstudiante() . "'>" . $g -> getNombre() .  " " . $g -> getApellido() .  "</option>";
								}
								}
								?>
							</select>
						</div>
		<button type="submit" name="registrar" class="btn btn-warning">Registrar</button> <br>
		<?php }else{
		    $error= 1;
		}?>
        				<a  class="font-italic" href="index.php?pid=<?php echo base64_encode("presentacion/sesionEstudiante.php")?>">Volver al Inicio</a> 					
        			</form>     
            	</div>
            </div>		
		</div>
	</div>
</div>

