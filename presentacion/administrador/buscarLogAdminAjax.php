<?php
$filtro = $_GET["filtro"];
$log = new LogAdmin();
$logs = $log -> consultarFiltro($filtro);
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-Black bg-warning">
					<h4>Consultar log de Admin</h4>
				</div>
				<div class="col">
				<div class="text-right"><?php echo count($logs) ?> registros encontrados</div>
				</div>
              	<div class="card-body">
					<table class="table table-hover table-striped table-responsive-md">
						<tr>
							<th>#</th>
							<th>Accion</th>
							<th>Datos</th>
							<th>Fecha</th>
							<th>Hora</th>
							<th>Administrador</th>
							
							
							
							
						</tr>
						<?php 
						$i=1;
						foreach($logs as $p){
						    
						    $a= new Administrador($p->getAdministrador());
						    $a->consultar();
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $p -> getAccion() . "</td>";
						    echo "<td>" . $p -> getDatos() . "</td>";
						    echo "<td>" . $p -> getFecha() . "</td>";
						    echo "<td>" . $p -> getHora() . "</td>";	
						    echo "<td>" . $a->getNombre() ." ". $a->getApellido() ."</td>";
						    echo "</div></td>";
						    
						    
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>