<?php
$proyecto = new Proyecto();
$cantidad = 5;
if(isset($_GET["cantidad"])){
    $cantidad = $_GET["cantidad"];
}
$pagina = 1;
if(isset($_GET["pagina"])){
    $pagina = $_GET["pagina"];
}
$proyectos = $proyecto -> consultarPaginacion($cantidad, $pagina);
$totalRegistros = $proyecto -> consultarCantidad();
$totalPaginas = intval($totalRegistros/$cantidad);
if($totalRegistros%$cantidad != 0){
    $totalPaginas++;
}
$ultimaPagina = ($totalPaginas == $pagina);
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-black bg-warning">
					<h4>Consultar Proyectos</h4>
				</div>
				<div class="col">
				<div class="text-left" >    Resultados <?php echo (($pagina-1)*$cantidad+1) ?> al <?php echo (($pagina-1)*$cantidad)+count($proyectos) ?> de <?php echo $totalRegistros ?> registros encontrados</div>
              	</div>
              	<div class="card-body">
					<table class="table table-hover table-striped table-responsive-lg">
						<tr>
							<th>#</th>
							<th>Titulo</th>
							<th>Descripcion</th>
							<th>Pfd</th>
							<th>Estudiante</th>
							<th>Tutor</th>
							<th>Jurado</th>
							<th>Servicios</th>
							
							
							
						</tr>
						<?php 
						$i=1;
						foreach($proyectos as $proyectoActual){
						    $proyecto = new Estudiante($proyectoActual->getEstudiante());
						    $proyecto->consultar();
						    if($proyectoActual->getTutor()!=""){
    						    $tutor = new Profesor($proyectoActual->getTutor());
    						    $tutor->consultar();
						    }
						    if($proyectoActual->getJurado()!=""){
    						    $jurado = new Profesor($proyectoActual->getjurado());
    						    $jurado->consultar();
						    }
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $proyectoActual -> getTitulo() . "</td>";
						    echo "<td>" . $proyectoActual -> getDescripcion() . "</td>";
						    echo "<td>" . (($proyectoActual -> getPdf()=="")?"Sin adjuntar":"<center><a class='fas fa-file-pdf' href='index.php?pid=" . base64_encode("presentacion/estudiante/verPDF.php") . "&idProyecto=" . $proyectoActual->getIdProyecto() . "' data-toggle='tooltip' data-placement='left' title='ver PDF'> </a>") . "</td>";
						    echo "<td>" . $proyecto->getNombre() . " ".$proyecto->getApellido() . "</td>";
						    echo "<td>" . (($proyectoActual -> getTutor()=="")?"Sin asignar":$tutor->getNombre() . " ".$tutor->getApellido()) . "</td>";
						    echo "<td>" . (($proyectoActual -> getJurado()=="")?"Sin asignar":$jurado->getNombre() . " ".$jurado->getApellido()) . "</td>";
						    echo "<td>" . "<a class='fas fa-file-pdf' target='_blank' href='procesoPDF.php?idProyecto=". $proyectoActual -> getIdProyecto() . "' data-toggle='tooltip' data-placement='left' title='Reporte del proceso'> </a>" . "</td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
					<div class="text-center">
        				<nav>
        					<ul class="pagination">
        						<li class="page-item <?php echo ($pagina==1)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/consultarProyectos.php") . "&pagina=" . ($pagina-1) . "&cantidad=" . $cantidad ?>"> &lt;&lt; </a></li>
        						<?php 
        						for($i=1; $i<=$totalPaginas; $i++){
        						    if($i==$pagina){
        						        echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
        						    }else{
        						        echo "<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("presentacion/administrador/consultarProyectos.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "'>" . $i . "</a></li>";
        						    }        						            						    
        						}        						
        						?>
        						<li class="page-item <?php echo ($ultimaPagina)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/consultarProyectos.php") . "&pagina=" . ($pagina+1) . "&cantidad=" . $cantidad ?>"> &gt;&gt; </a></li>
        					</ul>
        				</nav>
					</div>
					<select id="cantidad" >
						<option value="5" <?php echo ($cantidad==5)?"selected":"" ?>>5</option>
						<option value="10" <?php echo ($cantidad==10)?"selected":"" ?>>10</option>
						<option value="15" <?php echo ($cantidad==15)?"selected":"" ?>>15</option>
						<option value="20" <?php echo ($cantidad==20)?"selected":"" ?>>20</option>
					</select>
				</div>
            </div>
		</div>
	</div>
</div>

<script>
$("#cantidad").on("change", function() {
	url = "index.php?pid=<?php echo base64_encode("presentacion/administrador/consultarProyectos.php") ?>&cantidad=" + $(this).val(); 	
	location.replace(url);
});
</script>