<?php
$proyecto = new Proyecto();
$proyectos = $proyecto->consultarTodos();
?>
<div class="container mt-4">
	<div class="row">
		<div class="col-lg-8 col-md-8 col-12">
			 	<div class="card-body">
              		<script type="text/javascript">
                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawChart);
                    function drawChart() {
                      var data = google.visualization.arrayToDataTable([
                        ['Task', 'Hours per Day'],
                    	<?php 
                    			$aprobados=0;
                    			$noaprobados=0;
                    			foreach($proyectos as $p){
                    			    if($p -> getEstado()==1){
                                    $aprobados++; ?>
                    			   
                    		<?php	}else{
                    		         $noaprobados++;
                    		}
                    			}
                    		    ?>
                    		    ['Proyectos Aprobados', <?php echo $aprobados?>],
                    		    ['Proyectos No Aprobados', <?php echo $noaprobados?>]
                    		    //     ['Eat',      2],
                    		    //     ['Commute',  2],
                    		    //     ['Watch TV', 2],
                    		    //     ['Sleep',    7]
                    			  ]);
                      var options = {
                        title: 'Grafica de proyectos'
                      };
                      var chart = new google.visualization.PieChart(document.getElementById('piechart'));
                      chart.draw(data, options);
                    }
                    </script>
        			<div id="piechart" style="width: 900px; height: 500px;"></div>
    			</div>
    		</div>
    	</div>
    </div>