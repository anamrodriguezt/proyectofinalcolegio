<?php
$profesor= new Profesor($_GET["idProfesor"], "", "", "", "", $_GET["nuevoEstado"]);
$profesor -> cambiarEstado();
$profesor->consultar();
$accion=(($_GET["nuevoEstado"]==1)?"Aprobo":"Desaprobo");
$datos = "Profesor: ".$profesor->getNombre()." ".$profesor->getApellido();
$log = new LogAdmin("",$accion, $datos, date('Y-m-d'), date('H:i:s'),$_GET["idAdministrador"]);
$log->insertar();
echo ($_GET["nuevoEstado"]==1)?"<div id='icono" . $_GET["idProfesor"] . "'><span class='fas fa-check-circle' data-toggle='tooltip' data-placement='left' title='Habilitado'></span></div>":"<div id='icono" . $_GET["idProfesor"] . "'><span class='fas fa-times-circle' data-toggle='tooltip' data-placement='left' title='Deshabilitado'></span></div>";
?>
