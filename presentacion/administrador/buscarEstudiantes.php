<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
			<div class="card">
				<div class="card-header text-black bg-warning">
					<h4>Buscar Estudiante</h4>
				</div>
				<div class="card-body">
					<input type="text" id="filtro" class="form-control"
						placeholder="Palabra clave">
				</div>
			</div>
		</div>
	</div>
</div>
<div id="resultados"></div>
<script>
$(document).ready(function(){
    $("#filtro").keyup(function() {
        if($(this).val().length >= 3){            
	    	var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/buscarEstudiantesAjax.php") ?>&idAdministrador=<?php echo $_SESSION["id"] ?>&filtro=" + $(this).val();
    		$("#resultados").load(url);
        }
    });
});
</script>