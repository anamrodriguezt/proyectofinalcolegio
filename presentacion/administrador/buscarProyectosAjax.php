<?php
$filtro = $_GET["filtro"];
$proyecto = new Proyecto();
$proyectos = $proyecto -> consultarFiltro($filtro);
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-Black bg-warning">
					<h4>Consultar Proyectos</h4>
				</div>
				<div class="text-right"><?php echo count($proyectos) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped table-responsive-lg">
						<tr>
							<th>#</th>
							<th>Titulo</th>
							<th>Descripcion</th>
							<th>Pfd</th>
							<th>Estudiante</th>
							<th>Tutor</th>
							<th>Jurado</th>
							<th>Servicios</th>
							
							
						</tr>
						<?php 
						$i=1;
						foreach($proyectos as $proyectoActual){
						    $proyecto = new Estudiante($proyectoActual->getEstudiante());
						    $proyecto->consultar();
						    if($proyectoActual->getTutor()!=""){
    						    $tutor = new Profesor($proyectoActual->getTutor());
    						    $tutor->consultar();
						    }
						    if($proyectoActual->getJurado()!=""){
    						    $jurado = new Profesor($proyectoActual->getjurado());
    						    $jurado->consultar();
						    }
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $proyectoActual -> getTitulo() . "</td>";
						    echo "<td>" . $proyectoActual -> getDescripcion() . "</td>";
						    echo "<td>" . (($proyectoActual -> getPdf()=="")?"Sin adjuntar":"<center><a class='fas fa-file-pdf' href='index.php?pid=" . base64_encode("presentacion/estudiante/verPDF.php") . "&idProyecto=" . $proyectoActual->getIdProyecto() . "' data-toggle='tooltip' data-placement='left' title='ver PDF'> </a>") . "</td>";
						    echo "<td>" . $proyecto->getNombre() . " ".$proyecto->getApellido() . "</td>";
						    echo "<td>" . (($proyectoActual -> getTutor()=="")?"Sin asignar":$tutor->getNombre() . " ".$tutor->getApellido()) . "</td>";
						    echo "<td>" . (($proyectoActual -> getJurado()=="")?"Sin asignar":$jurado->getNombre() . " ".$jurado->getApellido()) . "</td>";
						    echo "<td>" . "<a class='fas fa-file-pdf' target='_blank' href='procesoPDF.php?idProyecto=". $proyectoActual -> getIdProyecto() . "' data-toggle='tooltip' data-placement='left' title='Reporte del proceso'> </a>" . "</td>";
						    
						    
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>