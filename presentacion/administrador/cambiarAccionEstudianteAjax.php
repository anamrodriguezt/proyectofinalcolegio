<?php
$idProfesor = $_GET["idEstudiante"];
$nuevoEstado = $_GET["nuevoEstado"];

echo "<a id='cambiarEstado" . $idProfesor . "' href='#' >" . (($nuevoEstado==1)?"<span class='fas fa-user-times' data-toggle='tooltip' data-placement='left' title='Deshabilitar'></span>":"<span class='fas fa-user-check' data-toggle='tooltip' data-placement='left' title='Habilitar'></span>") . "</a>";
?>

<script>
$(document).ready(function(){
	$("#cambiarEstado<?php echo $idProfesor ?>").click(function(e){
		$('[data-toggle="tooltip"]').tooltip('hide');
		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/cambiarEstadoEstudianteAjax.php") ?>&idEstudiante=<?php echo $idProfesor ?>&idAdministrador=<?php echo $_GET["idAdministrador"] ?>&nuevoEstado=<?php echo (($nuevoEstado==1)?"0":"1")?>";		
		$("#icono<?php echo $idProfesor ?>").load(url);
		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/cambiarAccionEstudianteAjax.php") ?>&idEstudiante=<?php echo $idProfesor ?>&idAdministrador=<?php echo $_GET["idAdministrador"] ?>&nuevoEstado=<?php echo (($nuevoEstado==1)?"0":"1")?>";
		$("#accion<?php echo $idProfesor ?>").load(url);
	});		
});
</script>
