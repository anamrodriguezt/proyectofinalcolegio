<?php
$filtro = $_GET["filtro"];
$proyecto = new Profesor();
$proyectos = $proyecto -> consultarFiltro($filtro);
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-Black bg-warning">
					<h4>Consultar Profesor</h4>
				</div>
				<div class="col">
				<div class="text-right"><?php echo count($proyectos) ?> registros encontrados</div>
				</div>
              	<div class="card-body">
					<table class="table table-hover table-striped table-responsive-md">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Correo</th>
							<th>Estado</th>
							<th>Servicios</th>
							
							
							
							
						</tr>
						<?php 
						$i=1;
						foreach($proyectos as $p){
						    
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $p -> getNombre() . "</td>";
						    echo "<td>" . $p -> getApellido() . "</td>";
						    echo "<td>" . $p -> getCorreo() . "</td>";
						    echo "<td>" . (($p -> getEstado()==1)?"<div id='icono" . $p -> getIdProfesor() . "'><span class='fas fa-check-circle' data-toggle='tooltip' data-placement='left' title='Habilitado'></span></div>":(($p -> getEstado()==0)?"<div id='icono" . $p -> getIdProfesor() . "'><span class='fas fa-times-circle' data-toggle='tooltip' data-placement='left' title='Deshabilitado'></span></div>":"<span class='fas fa-ban' data-toggle='tooltip' data-placement='left' title='Inactivo'></span>")) . "</td>";
						    echo "<td><div id='accion" . $p -> getIdProfesor() . "'><a id='cambiarEstado" . $p -> getIdProfesor() . "' href='#' >" . (($p-> getEstado()==1)?"<span class='fas fa-user-times' data-toggle='tooltip' data-placement='left' title='Deshabilitar'></span>":(($p -> getEstado()==0)?"<span class='fas fa-user-check' data-toggle='tooltip' data-placement='left' title='Habilitar'></span>":"")) . "</a>";
						    ?>
                        <script>
                        $(document).ready(function(){
                        	$("#cambiarEstado<?php echo $p -> getIdProfesor() ?>").click(function(e){
                        		$('[data-toggle="tooltip"]').tooltip('hide');
                        		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/cambiarEstadoProfesorAjax.php") ?>&idProfesor=<?php echo $p -> getIdProfesor() ?>&idAdministrador=<?php echo $_GET["idAdministrador"] ?>&nuevoEstado=<?php echo (($p -> getEstado()==1)?"0":"1")?>";		
                        		$("#icono<?php echo $p -> getIdProfesor() ?>").load(url);
                        		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/cambiarEstadoAccionAjax.php") ?>&idProfesor=<?php echo $p -> getIdProfesor() ?>&idAdministrador=<?php echo $_GET["idAdministrador"] ?>&nuevoEstado=<?php echo (($p -> getEstado()==1)?"0":"1")?>";
                        		$("#accion<?php echo $p -> getIdProfesor() ?>").load(url);
                        	});
                        });
                        </script>
						<?php   						    
						    echo "</div></td>";
						    
						    
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>