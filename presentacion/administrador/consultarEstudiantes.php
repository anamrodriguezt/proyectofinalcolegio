<?php
$proyecto= new Estudiante();
$cantidad = 5;
if(isset($_GET["cantidad"])){
    $cantidad = $_GET["cantidad"];
}
$pagina = 1;
if(isset($_GET["pagina"])){
    $pagina = $_GET["pagina"];
}
$proyectos = $proyecto -> consultarPaginacion($cantidad, $pagina);
$totalRegistros = $proyecto -> consultarCantidad();
$totalPaginas = intval($totalRegistros/$cantidad);
if($totalRegistros%$cantidad != 0){
    $totalPaginas++;
}
$ultimaPagina = ($totalPaginas == $pagina);
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-black bg-warning">
				
					<h4>Consultar Estudiantes</h4>
				</div>
				<div class="col">
				<div class="text-left" >    Resultados <?php echo (($pagina-1)*$cantidad+1) ?> al <?php echo (($pagina-1)*$cantidad)+count($proyectos) ?> de <?php echo $totalRegistros ?> registros encontrados</div>
              	</div>
              	<div class="card-body">
					<table class="table table-hover table-striped table-responsive-md">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Correo</th>
							<th>Codigo</th>
							<th>Estado</th>
							<th>Servicios</th>
							
							
							
							
						</tr>
						<?php 
						$i=1;
						foreach($proyectos as $p){
						    
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $p -> getNombre() . "</td>";
						    echo "<td>" . $p -> getApellido() . "</td>";
						    echo "<td>" . $p -> getCorreo() . "</td>";
						    echo "<td>" . $p -> getCodigo() . "</td>";
						    echo "<td>" . (($p -> getEstado()==1)?"<div id='icono" . $p -> getIdEstudiante() . "'><span class='fas fa-check-circle' data-toggle='tooltip' data-placement='left' title='Habilitado'></span></div>":(($p -> getEstado()==0)?"<div id='icono" . $p -> getIdEstudiante() . "'><span class='fas fa-times-circle' data-toggle='tooltip' data-placement='left' title='Deshabilitado'></span></div>":"<span class='fas fa-ban' data-toggle='tooltip' data-placement='left' title='Inactivo'></span>")) . "</td>";
						    echo "<td><div id='accion" . $p -> getIdEstudiante() . "'><a id='cambiarEstado" . $p -> getIdEstudiante() . "' href='#' >" . (($p-> getEstado()==1)?"<span class='fas fa-user-times' data-toggle='tooltip' data-placement='left' title='Deshabilitar'></span>":(($p -> getEstado()==0)?"<span class='fas fa-user-check' data-toggle='tooltip' data-placement='left' title='Habilitar'></span>":"")) . "</a>";
						    ?>
                        <script>
                        $(document).ready(function(){
                        	$("#cambiarEstado<?php echo $p -> getIdEstudiante() ?>").click(function(e){
                        		$('[data-toggle="tooltip"]').tooltip('hide');
                        		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/cambiarEstadoEstudianteAjax.php") ?>&idEstudiante=<?php echo $p -> getIdEstudiante() ?>&idAdministrador=<?php echo $_SESSION["id"] ?>&nuevoEstado=<?php echo (($p -> getEstado()==1)?"0":"1")?>";		
                        		$("#icono<?php echo $p -> getIdEstudiante() ?>").load(url);
                        		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/cambiarAccionEstudianteAjax.php") ?>&idEstudiante=<?php echo $p -> getIdEstudiante() ?>&idAdministrador=<?php echo $_SESSION["id"] ?>&nuevoEstado=<?php echo (($p -> getEstado()==1)?"0":"1")?>";
                        		$("#accion<?php echo $p -> getIdEstudiante() ?>").load(url);
                        	});
                        });
                        </script>
						<?php   						    
						    echo "</div></td>";
						    
						    
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
					<div class="text-center">
        				<nav>
        					<ul class="pagination">
        						<li class="page-item <?php echo ($pagina==1)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/consultarEstudiantes.php") . "&pagina=" . ($pagina-1) . "&cantidad=" . $cantidad ?>"> &lt;&lt; </a></li>
        						<?php 
        						for($i=1; $i<=$totalPaginas; $i++){
        						    if($i==$pagina){
        						        echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
        						    }else{
        						        echo "<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("presentacion/administrador/consultarEstudiantes.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "'>" . $i . "</a></li>";
        						    }        						            						    
        						}        						
        						?>
        						<li class="page-item <?php echo ($ultimaPagina)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/consultarEstudiante.php") . "&pagina=" . ($pagina+1) . "&cantidad=" . $cantidad ?>"> &gt;&gt; </a></li>
        					</ul>
        				</nav>
					</div>
					<select id="cantidad" >
						<option value="5" <?php echo ($cantidad==5)?"selected":"" ?>>5</option>
						<option value="10" <?php echo ($cantidad==10)?"selected":"" ?>>10</option>
						<option value="15" <?php echo ($cantidad==15)?"selected":"" ?>>15</option>
						<option value="20" <?php echo ($cantidad==20)?"selected":"" ?>>20</option>
					</select>
				</div>
            </div>
		</div>
	</div>
</div>

<script>
$("#cantidad").on("change", function() {
	url = "index.php?pid=<?php echo base64_encode("presentacion/administrador/consultarEstudiantes.php") ?>&cantidad=" + $(this).val(); 	
	location.replace(url);
});
</script>