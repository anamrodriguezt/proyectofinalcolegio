<?php
$proyecto = new Proyecto();
$proyectos = $proyecto->consultarTodos();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-black bg-warning">
					<h4>Consultar Proyectos</h4>
				</div>
				<div class="text-right"><?php echo count($proyectos) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped table-responsive-lg">
						<tr>
							<th>#</th>
							<th>Titulo</th>
							<th>Descripcion</th>
							<th>Pfd</th>
							<th>Estudiante</th>
							<th>Tutor</th>
							<th>Jurado</th>
							<th>Servicios</th>
							
						</tr>
						<?php 
						$i=1;
						foreach($proyectos as $proyectoActual){
						    $estudiante = new Estudiante($proyectoActual->getEstudiante());
						    $estudiante->consultar();
						    if($proyectoActual->getTutor()!=""){
						    $tutor = new Profesor($proyectoActual->getTutor());
						    $tutor->consultar();
						    }
						    if($proyectoActual->getJurado()!=""){
						    $jurado = new Profesor($proyectoActual->getjurado());
						    $jurado->consultar();
						    
						    }
						    
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $proyectoActual -> getTitulo() . "</td>";
						    echo "<td>" . $proyectoActual -> getDescripcion() . "</td>";
						    echo "<td>" . (($proyectoActual -> getPdf()=="")?"Sin adjuntar":"<center><a class='fas fa-file-pdf' href='index.php?pid=" . base64_encode("presentacion/estudiante/verPDF.php") . "&idProyecto=" . $proyectoActual -> getIdProyecto() . "' data-toggle='tooltip' data-placement='left' title='ver PDF'> </a>") . "</td>";
						    echo "<td>" . $estudiante->getNombre() . " ".$estudiante->getApellido() . "</td>";
						    echo "<td>" . (($proyectoActual -> getTutor()=="")?"<a href='index.php?pid=". base64_encode("presentacion/asignador/AsignarTutor.php") . "&idProyecto=" . $proyectoActual -> getIdProyecto(). "' data-toggle='tooltip' data-placement='left' title='Asignar Tutor'><span class='fas fa-address-book'></span></a>":$tutor->getNombre() . " ".$tutor->getApellido()) . "</td>";
						    echo "<td>" . (($proyectoActual -> getJurado()=="")?"<a href='index.php?pid=". base64_encode("presentacion/asignador/AsignarJurado.php") . "&idProyecto=" . $proyectoActual -> getIdProyecto(). "' data-toggle='tooltip' data-placement='left' title='Asignar Jurado'><span class='fas fa-address-book'></span></a>":$jurado->getNombre() . " ".$jurado->getApellido()) . "</td>";
						    echo "<td> <a href='index.php?pid=". base64_encode("presentacion/asignador/asignarTutor.php") . "&idProyecto=" . $proyectoActual -> getIdProyecto(). "' data-toggle='tooltip' data-placement='left' title='Editar Tutor'><span class='fas fa-edit'></span></a>
                                       <a href='index.php?pid=". base64_encode("presentacion/asignador/asignarJurado.php") . "&idProyecto=" . $proyectoActual -> getIdProyecto(). "' data-toggle='tooltip' data-placement='left' title='Editar Jurado'><span class='fas fa-edit'></span></a> 
                                       <a class='fas fa-file-pdf' target='_blank' href='procesoPDF.php?idProyecto=". $proyectoActual-> getIdProyecto() . "' data-toggle='tooltip' data-placement='left' title='Reporte del proceso'> </a>" . "</td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
					
				</div>
            </div>
		</div>
	</div>
</div>
