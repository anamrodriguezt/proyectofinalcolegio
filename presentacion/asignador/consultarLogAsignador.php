<?php
$asignador=new Asignador($_SESSION["id"]);
$asignador->consultar();
$log = new LogAsignador("","","","","",$_SESSION["id"]);
$logs = $log -> consultarlog();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-Black bg-warning">
					<h4>Consultar Log de Asignador: <?php echo $asignador->getNombre()." ". $asignador->getApellido() ?></h4>
				</div>
				<div class="col">
				<div class="text-right"><?php echo count($logs) ?> registros encontrados</div>
				</div>
              	<div class="card-body">
					<table class="table table-hover table-striped table-responsive-md">
						<tr>
							<th>#</th>
							<th>Accion</th>
							<th>Datos</th>
							<th>Fecha</th>
							<th>Hora</th>					
							
							
							
						</tr>
						<?php 
						$i=1;
						foreach($logs as $p){

						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $p -> getAccion() . "</td>";
						    echo "<td>" . $p -> getDatos() . "</td>";
						    echo "<td>" . $p -> getFecha() . "</td>";
						    echo "<td>" . $p -> getHora() . "</td>";	
						    echo "</div></td>";
						    
						    
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>