<?php
$proyecto = new Proyecto($_GET["idProyecto"]);
$proyecto -> consultar();
$titulo = $proyecto -> getTitulo();

if(isset($_POST["crear"])){
    $proyecto= new Proyecto($_GET["idProyecto"],"","","","","",$_POST["tutor"]);
    $proyecto -> insertarTutor();
    $profesor = new Profesor($_POST["tutor"]);
    $profesor -> consultar();
    $nombre= $profesor -> getNombre();
    $apellido= $profesor -> getApellido();
    $descripcion= "se asigno el tutor ".$nombre." ".$apellido." en el proyecto (".$titulo.")";
    $proceso = new Proceso("",$descripcion,$_SESSION["id"],$_SESSION["rol"],date('Y-m-d'),date('H:i:s'),$_GET["idProyecto"]);
    $proceso -> insertar();
    
    $asignador = new Asignador($_SESSION["id"]);
    $asignador->consultar();
    $accion= $profesor->getNombre()." ".$profesor->getApellido()." ha sido asignado como Tutor a: ".$titulo;
    $datos = "Asignador:".$asignador->getNombre()." ".$asignador->getApellido();
    $log = new LogAsignador("",$accion, $datos, date('Y-m-d'), date('H:i:s'),$_SESSION["id"]);
    $log->insertar();
}

?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-black bg-warning">
					<h4>Asignar Tutor</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Tutor Asignado
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php  } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/asignador/asignarTutor.php"). "&idProyecto=".$_GET["idProyecto"] ?>" method="post">
        				<div class="form-group">
    						<label>Profesor</label>
    					</div>
        				<div class="form-group">
    						<select class="form-control" name="tutor">
        						<?php 
        						$profesor= new Profesor();
        						$profesores = $profesor -> consultarTodos();
        						foreach($profesores as $c){
        						    if($c -> getIdProfesor()!= $proyecto -> getJurado()){
        						    echo '<option value="'. $c -> getIdProfesor() .'">' . $c -> getNombre()." " .$c -> getApellido().'</option>';        						    
        						    }
        						}
						?>               
                        </select>
    					</div>
        				<div class="form-group">
    						<button type="submit" name="crear" class="btn btn-warning">Asignar</button>
    					</div>	    					
        			</form>    

            	</div>
            </div>
		</div>
	</div>
</div>
