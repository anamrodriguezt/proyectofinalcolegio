<?php
require_once 'logica/Comentario.php';
require_once 'logica/Proyecto.php';

$proyecto = new Proyecto($_GET["idProyecto"]);
$proyecto = $proyecto->consultar();

$comentario= new Comentario("","",$_GET["idProyecto"]);
$comentarios= $comentario->consultarTodos();

?>
<div class="modal-header">
	<h5 class="modal-title">Comentarios y observaciones</h5>
	<button type="button" class="close" data-dismiss="modal"
		aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
</div>
<div class="modal-body">
	<table class="table table-striped table-hover">
		<tbody>
		<th width='5%'>#</th>
		<th width='50%'>Comentario</th>
		<th width='20%'>Fecha</th>
					<?php 
						$i=1;
						foreach($comentarios as $p){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>".$p-> getTexto() . "</td>";
						    echo "<td>".$p->getFecha() ."</td>";	
				            
				            echo "</tr>";
				            
						     $i++;
						}
						?>
		
		</tbody>
		
	</table>
</div>