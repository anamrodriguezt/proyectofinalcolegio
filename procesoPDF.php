<?php
require_once "logica/Proceso.php";
require_once "logica/Proyecto.php";
require_once "logica/Profesor.php";
require_once "logica/Asignador.php";
require_once "logica/Estudiante.php";
require_once "ezpdf/class.ezpdf.php";

$pdf = new Cezpdf("LETTER");
$pdf -> selectFont("ezpdf/fonts/Courier.afm");
$pdf -> ezSetCmMargins(2, 2, 3, 3);

$proceso = new Proceso("","","","","","",$_GET["idProyecto"]);
$procesos = $proceso -> consultarTodos();
$proyecto = new Proyecto($_GET["idProyecto"]);
$proyecto -> consultar();



$opciones = array("justification" => "center");
$pdf -> ezText("<b>Proceso del proyecto ".$proyecto -> getTitulo() ." </b>", 20, $opciones);
$pdf -> ezText("<b>Proceso</b>", 16, $opciones);

//$encabezados = array("<b>#</b>","<b>Nombre</b>","<b>Apellido</b>","<b>Correo</b>","<b>Estado</b>");
$encabezados = array(
    "num" => "<b>#</b>",
    "descripcion" => "<b>Descripcion</b>",
    "autor" => "<b>Autor</b>",
    "rol" => "<b>Rol</b>",
    "fecha" => "<b>Fecha</b>",
    "hora" => "<b>Hora</b>",
);
$datos = array();
$i = 0;
    $autor="";
    foreach ($procesos as $p){
        $autor="";
        if($p -> getRol()=="Estudiante"){
            $e=new Estudiante($p -> getAutor());
            $e -> consultar();
            $autor= $e -> getNombre()." ". $e -> getApellido();
        }else if($p -> getRol()=="Profesor"){
            $e=new Profesor($p -> getAutor());
            $e -> consultar();
            $autor= $e -> getNombre()." ". $e -> getApellido();
        }else{
            $e=new Asignador($p -> getAutor());
            $e -> consultar();
            $autor= $e -> getNombre()." ". $e -> getApellido();
        }
        $datos[$i]["num"] = $i + 1;
        $datos[$i]["descripcion"] = $p -> getDescripcion();
        $datos[$i]["autor"] = $autor;
        $datos[$i]["rol"] = $p -> getRol();
        $datos[$i]["fecha"] = $p -> getFecha();
        $datos[$i]["hora"] = $p -> getHora();
        $i++;
    }    


$opcionesTabla = array(
    "showLines" => 2,
    "shaded" => 1,
    "rowGap" => 3,
    "width"  => 500
);
$pdf -> ezSetDY(-20);
$pdf -> ezTable($datos, $encabezados, "Lista de  Procesos", $opcionesTabla);
$pdf -> ezStream();
// $pdfcode = $pdf->ezOutput();
// $fp=fopen("reportes/clientes.pdf",'wb');
// fwrite($fp,$pdfcode);
// fclose($fp);

?>
