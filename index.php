<?php

session_start();

require_once "logica/Administrador.php";

require_once "logica/Profesor.php";

require_once "logica/Estudiante.php";

require_once "logica/Asignador.php";

require_once 'logica/LogAdmin.php';

require_once 'logica/LogAsignador.php';

require_once 'logica/LogProfesor.php';

require_once 'logica/LogTutor.php';

require_once "logica/Proceso.php";

require_once "logica/Proyecto.php";

require_once "logica/LogEstudiante.php";

require_once "logica/Comentario.php";





$pid = "";

if(isset($_GET["pid"])){
    
    $pid = base64_decode($_GET["pid"]);
    
}else{
    
    $_SESSION["id"]="";
    
    $_SESSION["rol"]="";
    
}

?>

<html>



<head>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="icon" type="image/jpg" href="img/imagen.jpg" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> 
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" ></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>	
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>

	<script>

	$(function () {

		$('[data-toggle="tooltip"]').tooltip('hide');

	})

	</script>

</head>

<body class="p-3 mb-2 bg-light text-dark">

<?php 

$paginasSinSesion = array(

	"presentacion/autenticar.php",
   "presentacion/estudiante/registrarEstudiante.php"
);

if(in_array($pid, $paginasSinSesion)){

    include $pid;

}else if($_SESSION["id"]!="") {

    if($_SESSION["rol"] == "Administrador"){

        include "presentacion/menuAdministrador.php";

    }else if($_SESSION["rol"]== "Estudiante"){

        include "presentacion/menuEstudiante.php";

    }else if($_SESSION["rol"]== "Asignador"){

        include "presentacion/menuAsignador.php";

    }else if($_SESSION["rol"]== "Profesor"){

        include "presentacion/menuProfesor.php";

    }

    include $pid;

}else{

    include "presentacion/encabezado.php";

    include "presentacion/inicio.php";

}



?>	



</body>

</html>