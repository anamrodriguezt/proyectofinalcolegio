<?php

class ProyectoDAO{
    private $idProyecto;
    private $titulo;
    private $descripcion;
    private $pdf;
    private $estudiante;
    private $estudiante2;
    private $tutor;
    private $juez;
    private $estado;
    public function ProyectoDAO($idProyecto = "", $titulo = "", $descripcion = "", $pdf= "",$estudiante= "",$estudiante2= "", $tutor= "",$juez= "",$estado= ""){
        $this -> idProyecto = $idProyecto;
        $this -> titulo = $titulo;
        $this -> descripcion = $descripcion;
        $this -> pdf = $pdf;
        $this -> estudiante = $estudiante;
        $this -> estudiante2 = $estudiante2;
        $this -> tutor = $tutor;
        $this -> juez = $juez;
        $this -> estado= $estado;
        
    }

    public function consultar(){
        return "select id, titulo, descripcion, pdf,estudiante,estudiante2, tutor, jurado,estado
                from Proyecto
                where Id = '" . $this -> idProyecto .  "'";
    }
    
    public function consultarPJ(){
        return "select Id, titulo, descripcion, pdf,estudiante,estudiante2, tutor,  estado
                from Proyecto
                where jurado = '" . $this -> juez.  "'";
    }
    public function consultarPT(){
        return "select Id, titulo, descripcion, pdf,estudiante,estudiante2, jurado,  estado
                from Proyecto
                where tutor = '" . $this -> tutor.  "'";
    }
    
    public function consultarTodos(){
        return "select id, titulo, descripcion, pdf,estudiante,estudiante2, tutor, jurado,estado
                from Proyecto";
    }
    public function consultarTodos2(){
        return "select id, titulo, descripcion, pdf,estudiante,estudiante2, tutor, jurado,estado
                from Proyecto
                where estado=1";
    }
    public function consultar2(){
        return"select id, titulo, descripcion, pdf,estudiante,estudiante2, tutor, jurado,estado
                from Proyecto
                where estudiante = '" . $this -> estudiante.  "' or estudiante2= '" . $this -> estudiante.  "'";
    }
    public function consultar4($estudiante){
        return "select id, titulo, descripcion, pdf,estudiante,estudiante2, tutor, jurado,estado
                from Proyecto
                where estudiante = '" . $estudiante.  "' or estudiante2= '" . $estudiante.  "'";
    }
    public function consultarPaginacion($cantidad, $pagina){
        return "select Id,titulo, descripcion, pdf, estudiante,estudiante2, tutor, jurado,estado
                from Proyecto
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    public function consultarCantidad(){
        return "select count(Id)
                from Proyecto";
    }
    public function consultarP(){
        return "select count(Id)
                from Proyecto
                where Estudiante='". $this -> estudiante."' or Estudiante2='". $this -> estudiante."'";
    }
    public function insertarTutor(){
        return "update Proyecto
                set tutor = '" . $this -> tutor . "'
                where id = '" . $this -> idProyecto.  "'";
    }
    
    public function actualizar(){
        return "update proyecto 
                set titulo = '" . $this -> titulo. "',
                descripcion = '" . $this -> descripcion. "',
                pdf = '" . $this -> pdf. "'
                where estudiante ='" . $this -> estudiante."'";
    }
    public function insertarJurado(){
        return "update Proyecto
                set Jurado = '" . $this -> juez . "'
                where Id = '" . $this -> idProyecto.  "'";
    }
    
    public function insertar(){
        return "insert into Proyecto
                      (titulo, descripcion, pdf, estudiante".(($this->estudiante2==-1)?"":",estudiante2").", estado)
                   values ('" . $this->titulo. "', '" . $this->descripcion. "', '" . $this->pdf. "','" . $this->estudiante. "'".(($this->estudiante2==-1)?NULL:",'" . $this->estudiante2. "'").",'". 0 ."')";
    }
    
    public function cambiarEstado(){
        return "update proyecto 
                set estado = '" . $this -> estado . "'
                where id = '" . $this -> idProyecto .  "'";
    }
    public function consultarFiltro($filtro){
        return "select Id,titulo, descripcion, pdf, estudiante,estudiante2, tutor, jurado,estado
                from Proyecto
                where titulo like '%" . $filtro . "%' or estudiante =(select id from estudiante where nombre like '" . $filtro . "%' or apellido like '" . $filtro . "%')
                                                      or estudiante2 =(select id from estudiante where apellido like '" . $filtro . "%' or apellido like '" . $filtro . "%')
                                                      or tutor =(select id from profesor where nombre like '" . $filtro . "%' or apellido like '" . $filtro . "%')
                                                      or jurado =(select id from profesor where nombre like '" . $filtro . "%' or apellido like '" . $filtro . "%')
                                                      ";
    }
    
}

?>