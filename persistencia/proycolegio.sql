-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-03-2021 a las 16:13:22
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proycolegio`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `Id` int(11) NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  `Apellido` varchar(45) NOT NULL,
  `Correo` varchar(45) NOT NULL,
  `Clave` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`Id`, `Nombre`, `Apellido`, `Correo`, `Clave`) VALUES
(1, 'Jessica', 'Fernandez', '100@100.com', 'f899139df5e1059396431415e770c6dd');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignador`
--

CREATE TABLE `asignador` (
  `Id` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Apellido` varchar(45) DEFAULT NULL,
  `Correo` varchar(45) NOT NULL,
  `Clave` varchar(45) NOT NULL,
  `Estado` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `asignador`
--

INSERT INTO `asignador` (`Id`, `Nombre`, `Apellido`, `Correo`, `Clave`, `Estado`) VALUES
(2, 'Pedro', 'Garcia', '456@456.com', '250cf8b51c773f3f8dc8b4be867a9a02', 1),
(3, 'Omar', 'Arias', '10@10.com', 'd3d9446802a44259755d38e6d163e820', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario`
--

CREATE TABLE `comentario` (
  `Id` int(11) NOT NULL,
  `Texto` varchar(600) NOT NULL,
  `Proyecto` int(11) NOT NULL,
  `Fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudiante`
--

CREATE TABLE `estudiante` (
  `Id` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Apellido` varchar(45) DEFAULT NULL,
  `Correo` varchar(45) NOT NULL,
  `Clave` varchar(45) NOT NULL,
  `Codigo` int(20) NOT NULL,
  `Estado` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estudiante`
--

INSERT INTO `estudiante` (`Id`, `Nombre`, `Apellido`, `Correo`, `Clave`, `Codigo`, `Estado`) VALUES
(1, 'Diego', 'Gonzalez', '123@123.com', '202cb962ac59075b964b07152d234b70', 1321354765, 1),
(2, 'Luna', 'Lopez', '789@789.com', '68053af2923e00204c3ca7c6a3150cf7', 2012, 1),
(4, 'Karen', 'Ortiz', '1@1.com', 'c4ca4238a0b923820dcc509a6f75849b', 16374, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logadmin`
--

CREATE TABLE `logadmin` (
  `Id` int(11) NOT NULL,
  `Accion` varchar(200) NOT NULL,
  `Datos` varchar(200) NOT NULL,
  `Fecha` date NOT NULL,
  `Hora` time NOT NULL,
  `Administrador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logasignador`
--

CREATE TABLE `logasignador` (
  `Id` int(11) NOT NULL,
  `Accion` varchar(200) NOT NULL,
  `Datos` varchar(200) NOT NULL,
  `Fecha` date NOT NULL,
  `Hora` time NOT NULL,
  `asignador_Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logestudiante`
--

CREATE TABLE `logestudiante` (
  `Id` int(11) NOT NULL,
  `Accion` varchar(200) NOT NULL,
  `Datos` varchar(200) NOT NULL,
  `Fecha` date NOT NULL,
  `Hora` time NOT NULL,
  `Estudiante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logprofesor`
--

CREATE TABLE `logprofesor` (
  `Id` int(11) NOT NULL,
  `Autor` varchar(200) NOT NULL,
  `Datos` varchar(200) NOT NULL,
  `Fecha` date NOT NULL,
  `Hora` time NOT NULL,
  `Profesor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `logprofesor`
--

INSERT INTO `logprofesor` (`Id`, `Autor`, `Datos`, `Fecha`, `Hora`, `Profesor`) VALUES
(26, 'Desaprobo', 'Asignador: profesor 3', '2020-08-26', '19:20:18', 3),
(27, 'Aprobo', 'Asignador: profesor 3', '2020-08-26', '19:20:18', 3),
(28, 'Desaprobo', 'Asignador: profesor 3', '2020-08-26', '19:20:57', 3),
(29, 'Aprobo', 'Asignador: profesor 3', '2020-08-26', '19:21:01', 3),
(30, 'Desaprobo', 'Asignador: profesor 3', '2020-08-26', '19:21:03', 3),
(31, 'Aprobo', 'Asignador: profesor 3', '2020-08-26', '19:21:06', 3),
(32, 'Desaprobo', 'Asignador: profesor 3', '2020-08-26', '19:21:06', 3),
(33, 'Aprobo', 'Asignador: profesor 3', '2020-08-26', '19:21:07', 3),
(34, 'Desaprobo', 'Asignador: profesor 3', '2020-08-26', '19:21:07', 3),
(35, 'Aprobo', 'Asignador: profesor 3', '2020-08-26', '19:21:08', 3),
(36, 'Desaprobo', 'Asignador: profesor 3', '2020-08-26', '19:21:08', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso`
--

CREATE TABLE `proceso` (
  `Id` int(11) NOT NULL,
  `Descripcion` varchar(45) NOT NULL,
  `Autor` varchar(45) NOT NULL,
  `Rol` varchar(45) NOT NULL,
  `Fecha` date NOT NULL,
  `Hora` time NOT NULL,
  `proyecto_Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor`
--

CREATE TABLE `profesor` (
  `Id` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Apellido` varchar(45) DEFAULT NULL,
  `Correo` varchar(45) NOT NULL,
  `Clave` varchar(45) NOT NULL,
  `Estado` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `profesor`
--

INSERT INTO `profesor` (`Id`, `Nombre`, `Apellido`, `Correo`, `Clave`, `Estado`) VALUES
(1, 'Pedro', 'Suarez', '500@500.com', 'cee631121c2ec9232f3a2f028ad5c89b', 1),
(2, 'Martin', 'Gomez', '60@60.com', '072b030ba126b2f4b2374f342be9ed44', 1),
(3, 'profesor', '3', '3@3.com', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

CREATE TABLE `proyecto` (
  `Id` int(11) NOT NULL,
  `Titulo` varchar(200) NOT NULL,
  `Descripcion` varchar(200) NOT NULL,
  `Pdf` varchar(45) NOT NULL,
  `Estudiante` int(11) NOT NULL,
  `Estudiante2` int(11) DEFAULT NULL,
  `Tutor` int(11) DEFAULT NULL,
  `Jurado` int(11) DEFAULT NULL,
  `Estado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `asignador`
--
ALTER TABLE `asignador`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `comentario`
--
ALTER TABLE `comentario`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `fk_comentario_proyecto1_idx` (`Proyecto`);

--
-- Indices de la tabla `estudiante`
--
ALTER TABLE `estudiante`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `logadmin`
--
ALTER TABLE `logadmin`
  ADD PRIMARY KEY (`Id`,`Administrador`),
  ADD KEY `fk_logAdmin_administrador1_idx` (`Administrador`);

--
-- Indices de la tabla `logasignador`
--
ALTER TABLE `logasignador`
  ADD PRIMARY KEY (`Id`,`asignador_Id`),
  ADD KEY `fk_logAsignador_asignador1_idx` (`asignador_Id`);

--
-- Indices de la tabla `logestudiante`
--
ALTER TABLE `logestudiante`
  ADD PRIMARY KEY (`Id`,`Estudiante`),
  ADD KEY `fk_logEstudiante_estudiante1_idx` (`Estudiante`);

--
-- Indices de la tabla `logprofesor`
--
ALTER TABLE `logprofesor`
  ADD PRIMARY KEY (`Id`,`Profesor`),
  ADD KEY `fk_logProfesor_profesor1_idx` (`Profesor`);

--
-- Indices de la tabla `proceso`
--
ALTER TABLE `proceso`
  ADD PRIMARY KEY (`Id`,`proyecto_Id`),
  ADD KEY `fk_proceso_proyecto1_idx` (`proyecto_Id`);

--
-- Indices de la tabla `profesor`
--
ALTER TABLE `profesor`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD PRIMARY KEY (`Id`,`Estudiante`),
  ADD KEY `fk_proyecto_estudiante1_idx` (`Estudiante`),
  ADD KEY `fk_proyecto_profesor1_idx` (`Tutor`),
  ADD KEY `fk_proyecto_profesor2_idx` (`Jurado`),
  ADD KEY `fk_proyecto_estudiante2_idx` (`Estudiante2`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `asignador`
--
ALTER TABLE `asignador`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `comentario`
--
ALTER TABLE `comentario`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `estudiante`
--
ALTER TABLE `estudiante`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `logadmin`
--
ALTER TABLE `logadmin`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `logasignador`
--
ALTER TABLE `logasignador`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `logestudiante`
--
ALTER TABLE `logestudiante`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `logprofesor`
--
ALTER TABLE `logprofesor`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `proceso`
--
ALTER TABLE `proceso`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT de la tabla `profesor`
--
ALTER TABLE `profesor`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comentario`
--
ALTER TABLE `comentario`
  ADD CONSTRAINT `fk_comentario_proyecto1` FOREIGN KEY (`Proyecto`) REFERENCES `proyecto` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `logadmin`
--
ALTER TABLE `logadmin`
  ADD CONSTRAINT `fk_logAdmin_administrador1` FOREIGN KEY (`Administrador`) REFERENCES `administrador` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `logasignador`
--
ALTER TABLE `logasignador`
  ADD CONSTRAINT `fk_logAsignador_asignador1` FOREIGN KEY (`asignador_Id`) REFERENCES `asignador` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `logestudiante`
--
ALTER TABLE `logestudiante`
  ADD CONSTRAINT `fk_logEstudiante_estudiante1` FOREIGN KEY (`Estudiante`) REFERENCES `estudiante` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `logprofesor`
--
ALTER TABLE `logprofesor`
  ADD CONSTRAINT `fk_logProfesor_profesor1` FOREIGN KEY (`Profesor`) REFERENCES `profesor` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proceso`
--
ALTER TABLE `proceso`
  ADD CONSTRAINT `fk_proceso_proyecto1` FOREIGN KEY (`proyecto_Id`) REFERENCES `proyecto` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD CONSTRAINT `fk_proyecto_estudiante1` FOREIGN KEY (`Estudiante`) REFERENCES `estudiante` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_proyecto_estudiante2` FOREIGN KEY (`Estudiante2`) REFERENCES `estudiante` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_proyecto_profesor1` FOREIGN KEY (`Tutor`) REFERENCES `profesor` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_proyecto_profesor2` FOREIGN KEY (`Jurado`) REFERENCES `profesor` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
